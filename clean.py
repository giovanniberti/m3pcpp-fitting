import csv
from pathlib import Path

DATASET_PATH = Path("scenedet-results")
DATASET_OUTPUT_PATH = Path("clean")

DATASET_OUTPUT_PATH.mkdir(exist_ok=True)
datasets = DATASET_PATH.glob("*.csv")


def grouper(iterable, n):
    """Collect data into fixed-length chunks or blocks"""
    args = [iter(iterable)] * n
    return zip(*args)


for dataset in datasets:
    output_path = DATASET_OUTPUT_PATH / dataset.name
    with open(dataset) as ds, open(output_path, 'w') as output:
        reader = csv.reader(ds)
        writer = csv.writer(output)
        for record in reader:
            record = record[3:]
            sequence = grouper(record, 2)
            output_sequence = []
            previous_timestamp = 0
            for e, ts in sequence:
                ts = int(ts)
                output_sequence.append(e)
                output_sequence.append(str(ts - previous_timestamp))
                previous_timestamp = ts
            writer.writerow(output_sequence)
