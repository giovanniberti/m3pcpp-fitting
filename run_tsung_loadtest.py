import argparse
from pathlib import Path
import subprocess
from datetime import datetime, timedelta
import sys
from tsung_gen import generate_tsung_conf

parser = argparse.ArgumentParser()
parser.add_argument('-d', '--result_dir', required=True, type=Path)
args = parser.parse_args()

for run_dir in filter(lambda d: d.is_dir(), args.result_dir.iterdir()):
    tsung_data_dirs = [d for d in run_dir.iterdir() if d.is_dir()]

    for tsung_data_dir in tsung_data_dirs:
        generated_run_sequence = tsung_data_dir.parent / (str(tsung_data_dir.stem) + ".csv")
        old_run_dirs = [d for d in tsung_data_dir.iterdir() if d.is_dir()]

        if not old_run_dirs:
            continue
        elif len(old_run_dirs) > 1:
            print(f"Found multiple old_run_dirs: {old_run_dirs}")

        old_run_dir = old_run_dirs[0]

        if old_run_dir.stem[0] != "_":
            print(
                f"Replacing {old_run_dir} with {old_run_dir.parent / ('_' + old_run_dir.stem)}")
            old_run_dir.replace(old_run_dir.parent / ("_" + old_run_dir.stem))

        generation_paths = generate_tsung_conf(dataset_path=generated_run_sequence.parent,
            output_path=generated_run_sequence.parent,
            run_time=timedelta(minutes=15))
        print(f"Generated tsung.xml in {generation_paths}")

        for path in generation_paths:
            print(f"Starting Tsung load test ({tsung_data_dir.resolve()})...")
            sys.stdout.flush()

            tsung_start_time = datetime.utcnow()
            subprocess.run([
                'docker',
                'run',
                '-it',
                '--network=scenario-detection_default',
                '--mount',
                f'type=bind,source={path.resolve()},destination=/usr/local/tsung',
                'tsung',
                '-f',
                '/usr/local/tsung/tsung.xml',
                'start'
            ],
            check=True)
            break

