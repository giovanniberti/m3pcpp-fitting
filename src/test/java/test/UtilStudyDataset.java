package test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class UtilStudyDataset {




	public static void main(String[] args) {


		/*  datasetEvents
		 *  src//main//resources//dataset//
		*/
		String endFile = "src//main//resources//dataset//m_2_6_data.csv";
		String line = "";
		
		double min=10000.;
		double minSeq=100.;
		try (BufferedReader br = new BufferedReader(new FileReader(endFile))) {
			int sumI=0;
			double sumD=0.;
		
			int numLines=0;
			int past=0;
			double pastD=0.;
				while ((line = br.readLine()) != null) {
					numLines++;
					String[] pieces = line.split(",");

					for(int i=0; i<pieces.length;i=i+3) {
						sumI++;
						sumD+= Double.parseDouble(pieces[i]);
						min = (min<Double.parseDouble(pieces[i]))? min : Double.parseDouble(pieces[i]);
					}
					minSeq= (minSeq< (sumD-pastD))? minSeq : (sumD-pastD);
					System.out.println(sumI-past);
					System.out.println(sumD-pastD);
					past=sumI;
					pastD=sumD;
					
				}
				
				System.out.println(min);
				System.out.println(minSeq);
				System.out.println("number events average "+sumI/(double)numLines);
				System.out.println("time average "+sumD/(double)numLines);

			} catch (IOException e) {
				e.printStackTrace();
			}


		


	}
}
