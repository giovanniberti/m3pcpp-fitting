package test;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.SortedSet;
import java.util.TreeSet;



public class CombinedOperationsForGnuplot {

	public static double Z=1.96;

	public static int INSTANCE=20;



	public static void main(String[] args) {


		//salvataggio di 3 file per documento di predizione: descrizione TP,TN,FP,FN,Precision,Recall ; 
		//file per gnuplot ; calcolo dell' AUC


		//		File dir = new File("src//predictions");
		//		File[] directoryListing = dir.listFiles();
		//		if (directoryListing != null) {
		//			for (File child : directoryListing) {
		//				// Do something with child
		//
		//
		//
		//				String currentName= child.getName();


		int macrostatesNumber=-1;
		for(int model=0;model<3;model++) {

			if(model==0 || model==1) {
				macrostatesNumber=5;
			}
			if(model==2) {
				macrostatesNumber=10;
			}

			for(int dataDim=0;dataDim<3;dataDim++) {

				
				HashSet<Integer> numCPH = new HashSet<>();
				numCPH.add(1);
				numCPH.add(3);
				for(int numInCPH: numCPH) {


					for(int ex=0;ex<1;ex++) {
						String k="";
						if(ex==0) {
							k="_v5.csv";
						}else if(ex==1) {
							k="_v4.csv";
						}else if(ex==2) {
							k="_v8.csv";
						}

						double[][] precisions = new double[INSTANCE][1000];
						int tot=0;
						for(int instance=0;instance<INSTANCE;instance++) {



							String root="model_m"+model+"_i"+instance+"_dd"+dataDim+"_m"+macrostatesNumber+"_c"+numInCPH+k;
						
							
							
							String currentName =root;

							String in_threshold="src//predictions//"+currentName;
							String out_threshold="src//results//"+"t_"+currentName;


							SortedSet<Double> sortS = new TreeSet<>();
							double minT=1.;

							System.out.println(in_threshold);

							String line = "";
							try (BufferedReader br = new BufferedReader(new FileReader(in_threshold))) {

								while ((line = br.readLine()) != null) {

									String[] pieces = line.split(",");

									if(pieces.length>=2) {
										for(int i=0; i<pieces.length;i=i+2) {
											double val =Double.parseDouble(pieces[i]);
											sortS.add(-val);
											if(Integer.parseInt(pieces[i+1])==1) {

												minT = (minT>val)? val : minT;
											}
										}
									}

								}
							} catch (IOException e) {
								e.printStackTrace();
							}



							File file = new File(out_threshold);
							try (PrintWriter writer = new PrintWriter(file)) {

								writer.println("Threshold,TP,FP,TN,FN,Pr,Re,F1");

								int index=0;
								for(Double thrNeg: sortS) {
									double thr = - thrNeg;
									
									if(thr>minT) {

										line = "";

										int TP =0;
										int FP =0;
										int FN =0;
										int TN =0;

										//sto riaprendo tante volte lo stesso file e rileggendo gli stessi valori, 
										//ma sono 100*100 al max quindi pace, non mi va di salvare esterno
										try (BufferedReader br = new BufferedReader(new FileReader(in_threshold))) {

											while ((line = br.readLine()) != null) {

												String[] pieces = line.split(",");

												if(pieces.length>=2) {
													for(int i=0; i<pieces.length;i=i+2) {
														if(thr<= Double.parseDouble(pieces[i])) {
															if(Integer.parseInt(pieces[i+1])==1) {
																TP++;
															}else{
																FP++;
															}
														}else {
															if(Integer.parseInt(pieces[i+1])==1) {
																FN++;
															}else{
																TN++;
															}
														}

													}
												}

											}

											double P= TP/(double)(TP+FP);
											double R= TP/(double)(TP+FN);
											double F= 2*P*R/(P+R);


											if(R> index*0.01) {

												precisions[instance][index]=P;
												writer.println(thr+","+TP+","+FP+","+TN+","+FN+","+P+","+R+","+F);
												index++;
											}
											if(R>0.99) {
												tot=index;
												break;
												
											}

										} catch (IOException e) {
											e.printStackTrace();
										}

									}


								}
							} catch (FileNotFoundException e) {
								System.out.println(e.getMessage());
							}



							String in_gp_title=out_threshold;
							String out_gp_title="src//results//"+"gp_"+currentName;

							line = "";

							file = new File(out_gp_title);
							try (PrintWriter writer = new PrintWriter(file)) {

								int counter=0;

								try (BufferedReader br = new BufferedReader(new FileReader(in_gp_title))) {

									while ((line = br.readLine()) != null) {
										if(counter>0) {


											String[] pieces = line.split(",");
											if(pieces.length>0) {
												writer.println(pieces[5]+" "+pieces[6]);
											}

										}
										counter++;

									}

								} catch (IOException e) {
									e.printStackTrace();
								}


							} catch (FileNotFoundException e) {
								System.out.println("errore");
								System.out.println(e.getMessage());
							}

							String in_AUC_title=out_gp_title;
							String out_AUC_title="src//results//"+"AUC_"+currentName;


							line = "";

							double sum=0.;

							file = new File(out_AUC_title);
							try (PrintWriter writer = new PrintWriter(file)) {

								try (BufferedReader br = new BufferedReader(new FileReader(in_AUC_title))) {

									double x=0.;
									double y=1.;
									double oldx;
									double oldy;

									while ((line = br.readLine()) != null) {

										oldx=x;
										oldy=y;

										String[] pieces = line.split(" ");
										if(pieces.length>0) {
											x=Double.parseDouble(pieces[0]);
											y=Double.parseDouble(pieces[1]);
											if(y>oldy) {
												sum+=(x-oldx)*(y-oldy)/2;
											}
											//potevo lasciare eq di sopra senza if
											if(x<oldx) {
												sum-=(x-oldx)*(oldy-y)/2;
											}

											sum+= (x-oldx)*oldy ;
										}

									}
									writer.write(Double.toString(sum));

								} catch (IOException e) {
									e.printStackTrace();
								}


							} catch (FileNotFoundException e) {
								System.out.println("errore");
								System.out.println(e.getMessage());
							}



						}

						////AVERAGE


						double[] mean= new double[tot];
						double[] se= new double[tot];

						for(int in=0;in<INSTANCE;in++) {
							for(int i=0;i<tot;i++) {
								mean[i]+=precisions[in][i];
							}
						}
						for(int i=0;i<tot;i++) {
							mean[i]/= INSTANCE;
						}

						for(int i=0;i<tot;i++) {
							for(int in=0;in<INSTANCE;in++) {
								se[i]+= (precisions[in][i]-mean[i])*(precisions[in][i]-mean[i]);
							}
							se[i]/= INSTANCE;
							se[i]= Math.sqrt(se[i]);
							
						}
						for(int i=0;i<tot;i++) {
							se[i]/= Math.sqrt(INSTANCE);
						}


						String root="aver"+"_m"+model+"_dd"+dataDim+"_m"+macrostatesNumber+"_c"+numInCPH+k;

						String averagePath="src//results//"+root;

						File file = new File(averagePath);
						String line="";
						try (PrintWriter writer = new PrintWriter(file)) {

							for(int i=0;i<tot;i++) {
								writer.println(i*0.01+" "+mean[i]+" "+(mean[i]-se[i]*Z)+" "+(mean[i]+se[i]*Z));
							}


						} catch (FileNotFoundException e) {
							System.out.println("errore");
							System.out.println(e.getMessage());
						}

						String in_AUC_title=averagePath;
						String out_AUC_title="src//results//"+"AUC_"+root;


						line = "";

						double sum=0.;

						file = new File(out_AUC_title);
						try (PrintWriter writer = new PrintWriter(file)) {

							try (BufferedReader br = new BufferedReader(new FileReader(in_AUC_title))) {

								double x=0.;
								double y=1.;
								double oldx;
								double oldy;

								while ((line = br.readLine()) != null) {

									oldx=x;
									oldy=y;

									String[] pieces = line.split(" ");
									if(pieces.length>0) {
										x=Double.parseDouble(pieces[0]);
										y=Double.parseDouble(pieces[1]);
									
										sum+= (x-oldx)*oldy ;
									}

								}
								writer.write(Double.toString(sum));

							} catch (IOException e) {
								e.printStackTrace();
							}


						} catch (FileNotFoundException e) {
							System.out.println("errore");
							System.out.println(e.getMessage());
						}


					}




				}


			}
		}
	}

}
