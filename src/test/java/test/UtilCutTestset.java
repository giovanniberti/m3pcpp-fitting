package test;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;


public class UtilCutTestset {


	private static double STEP=3;
	//deve lasciare spazio perch� spostandomi un po avanti mi avanzi lo stesso 
	public static double randomSpaceExtra =22*STEP;

	/**
	 * Produce tagli del testSet nel formato   
	 * 
	 * tempo,evento, .... , tempo, firstEnd
	 * tempoUltimoEventoPrimaPredizione, tempoAlFallimentoDaStart
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		String endS="END";

		for(int model=0;model<3;model++) {



			for(int instance=0;instance<20;instance++) {

				String test="src\\main\\resources\\dataset\\"+"m_"+model+"_"+instance+"_test";

				String outputCut=test+"_cut";


				outputCut+=".csv";
				test+=".csv";



				String line = "";

				File file = new File(outputCut);
				try (PrintWriter writer = new PrintWriter(file)) {


					try (BufferedReader br = new BufferedReader(new FileReader(test))) {

						while ((line = br.readLine()) != null) {
							String[] pieces = line.split(",");

							double endTime=0.;
							int endIndex=0;



							for(int i=0; i<pieces.length;i=i+3) {

								if(pieces[i+2].equals(endS) && endIndex==0) {
									endIndex=i;
									endTime +=  Double.parseDouble(pieces[i]);
								}else if(!pieces[i+2].equals(endS)){
									endTime +=  Double.parseDouble(pieces[i]);
								}
							}

							double randTime= Math.random()*(endTime-randomSpaceExtra);

							if(randTime<0) {
								System.out.println("random minore di 0, "+randTime);
								randTime=0.;
							}
							System.out.println("end= "+endTime+"chosen "+randTime+"remaining"+ (endTime - randTime));
							endTime = endTime - randTime;

							double sum=0.;
							boolean started =false;
							for(int i=0; i<endIndex+1;i=i+3) {
								sum+= Double.parseDouble(pieces[i]);
								if(sum>randTime) {
									if(!started) {
										started=true;
										writer.write(sum-randTime+","+pieces[i+1]+","+pieces[i+2]+",");
									}else {
										writer.write(pieces[i]+","+pieces[i+1]+","+pieces[i+2]+",");
									}
								}
							}
							writer.println();
							writer.write(""+endTime);
							writer.println();


						}

					} catch (IOException e) {
						e.printStackTrace();
					}


				} catch (FileNotFoundException e) {
					System.out.println("errore");
					System.out.println(e.getMessage());
				}


			}
		}
	}




}
