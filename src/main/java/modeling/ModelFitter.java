package modeling;

import java.util.List;

import org.ejml.simple.SimpleMatrix;

public interface ModelFitter {

	
	public SimpleMatrix getM();
	public SimpleMatrix getP1espanso();
	public double[][] getGaussianMeans();
	public double[][] getGaussianVariances();
	public int[] getIndexOfCphForState();
	public double[] getInitialProbabilities();
	public int getTotalStatesNumber();
 	public List<String> getListEvents();
 	
 	public SimpleMatrix getD0();
 	
 	public double[] getProbOfBeingInState();
 	
 	
 	public SimpleMatrix unifP0loweredExponent(double i);
	
 	public SimpleMatrix unifP0normalExponent(double i);
 	
 		
	
 	public double getLogFitness();
 	
 	public void print(String s);
	
	
}
