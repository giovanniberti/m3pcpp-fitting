package modeling;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import modeling.InputTriple;
import org.apache.commons.math3.random.RandomDataGenerator;
import org.ejml.simple.SimpleMatrix;

public class ModelSimulator {
    private final int statesN;
    private final SimpleMatrix D0;
    private final SimpleMatrix D1;
    private final SimpleMatrix C;
    private final ArrayList<String> eventNames;
    private final double[] stateDistribution;

    public ModelSimulator(SimpleMatrix D0, SimpleMatrix D1, SimpleMatrix C, double[] stateDistribution, List<String> eventNames) {
        assert D0.numRows() == D0.numCols();
        assert D1.numRows() == D1.numCols();
        assert D0.numCols() == D1.numCols();
        assert stateDistribution.length == D0.numCols();
        assert D0.numCols() == C.numRows();
        assert eventNames.size() == C.numCols();

        this.D0 = D0;
        this.D1 = D1;
        this.C = C;
        this.eventNames = new ArrayList<>(eventNames);
        this.stateDistribution = stateDistribution;
        this.statesN = stateDistribution.length;
    }

    public List<InputTriple> simulateRun() {
        double[] stateLambdas = new double[statesN];
        for (int i = 0; i < statesN; i++) {
            stateLambdas[i] = Math.abs(D0.get(i, i));
        }
        System.out.println("D0:\n" + D0);
        System.out.println("D1:\n" + D1);
        System.out.println("stateLambdas: " + Arrays.toString(stateLambdas));

        double[][] transitionProbabilities = new double[statesN][statesN];
        double[] emissionThresholds = new double[statesN];

        for (int i = 0; i < statesN; i++) {
            for (int j = i + 1; j < statesN; j++) {
                transitionProbabilities[i][j] = D0.get(i, j) / stateLambdas[i];
                emissionThresholds[i] += D0.get(i, j) / stateLambdas[i];
            }
        }
        SimpleMatrix _transitionProbabilities = new SimpleMatrix(transitionProbabilities);
        System.out.println("transProb:\n" + _transitionProbabilities);
        System.out.println("emissionThrehsolds:\n" + Arrays.toString(emissionThresholds));

        RandomDataGenerator rng = new RandomDataGenerator();
        ArrayList<InputTriple> resultSequence = new ArrayList<>();

        //index over states, represent the state under simulation at the moment, changes over the path
        int currentState = 0;

        double rand = Math.random();

        double chooseState = stateDistribution[0];
        while (rand > chooseState) {
            currentState++;
            chooseState += stateDistribution[currentState];
        }

        double sequenceTime = 0;
        double lastTimestamp = 0;
        while (currentState != statesN - 1) {

            double sojournTime = rng.nextExponential(1 / stateLambdas[currentState]);
            sequenceTime += sojournTime;

            rand = Math.random();

            int next = currentState;
            chooseState = 0.0;

            if (rand > emissionThresholds[currentState]) {

                SimpleMatrix typeDist = C.extractVector(true, currentState);
                rand = Math.random();
                double chooseType = typeDist.get(0);
                int type = 0;

                while (rand > chooseType) {
                    type++;
                    chooseType += typeDist.get(type);
                }

                InputTriple e = new InputTriple(sequenceTime - lastTimestamp, 0.0, eventNames.get(type));
                resultSequence.add(e);
                System.out.println("emitting " + e);

                lastTimestamp = sequenceTime;
            } else {
                while (rand > chooseState) {
                    next++;
                    chooseState += transitionProbabilities[currentState][next];
                }
            }

            currentState = next;
        }

        return resultSequence;
    }

    private static int sampleDistribution(SimpleMatrix probabilities) {
        return sampleDistribution(0.0, probabilities);
    }

    private static int sampleDistribution(double initialProbability, SimpleMatrix probabilities) {
        double cumulativeProbability = Math.random();
        int next = 0;
        double cumulatedProbability = initialProbability;

        while (cumulativeProbability > cumulatedProbability) {
            next++;
            cumulatedProbability += probabilities.get(next);
        }

        return next;
    }
}
