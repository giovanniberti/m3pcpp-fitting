package modeling;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.util.*;

import com.google.common.collect.Streams;
import dataset.DatasetLoader;
import org.apache.commons.cli.*;
import org.apache.commons.math3.util.Pair;

public class Main {

    public Main() {}


    public final static int MAX_OPS = 3;


    public static double STEP = 3;

    public static int CUTS = 20; //20


    @SuppressWarnings("null")
    public static void main(String[] args) throws IOException {
        Options options = new Options();
        Option datasetRootOption = new Option("d", "dataset", true, "dataset directory. Must contain files named `num`_data.csv and `num`_test.csv, where `num` goes from 0 to the number of cuts.");
        datasetRootOption.setRequired(true);
        options.addOption(datasetRootOption);

        Option macroStatesOption = new Option("m", "macrostates", true, "number of macrostates");
        macroStatesOption.setRequired(true);
        options.addOption(macroStatesOption);

        Option datasetCuts = new Option("k", "cuts", true, "number of dataset cuts");
        datasetCuts.setRequired(true);
        options.addOption(datasetCuts);

        Option outputDirectory = new Option("o", "output", true, "output directory for learned parameters");
        options.addOption(outputDirectory);

        Option simulatedSequences = new Option("n", "sequences", true, "number of sequences to simulate with learned parameters");
        options.addOption(simulatedSequences);

        Option runId = new Option("i", "id", true, "identifier for run. If omitted the default value is the ISO-8601 date of the start of the program execution.");
        options.addOption(runId);

        Option dampeningFactorOpt = new Option("f", "dampening", true, "Dampening factor for uniformization. Default value = 1");
        options.addOption(dampeningFactorOpt);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd = null;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("m3cpp-fitting", options);

            System.exit(1);
        }

        Instant startTime = Instant.now();
        System.out.println("Fitting started at " + startTime);

        String pathDatasetRoot = cmd.getOptionValue("dataset");
        Path pathDataset;
        Path pathTestset;

        Path outputPath = null;
        if (cmd.getOptionValue("output") != null) {
            String outputFolder = cmd.getOptionValue("id", startTime.toString());

            outputPath = Paths.get(cmd.getOptionValue("output"), outputFolder);
            Files.createDirectories(outputPath);
        }

        CUTS = Integer.parseInt(cmd.getOptionValue("cuts"));

        int sequencesToSimulate = Integer.parseInt(cmd.getOptionValue("sequences", "0"));

        if (sequencesToSimulate > 0 && outputPath == null) {
            System.err.println("Asked to simulate sequences without output path!");
            System.exit(1);
        }

        int macrostatesNumber = Integer.parseInt(cmd.getOptionValue("macrostates"));
        double dampeningFactor = Double.parseDouble(cmd.getOptionValue("dampening", "1.0"));
        for (int cut = 0; cut < CUTS; cut++) {
            System.out.println("=== Starting fitting on " + pathDatasetRoot + " (" + (cut + 1) + "/" + CUTS + ") ===");

            pathDataset = Paths.get(pathDatasetRoot, cut + "_data.csv");
            pathTestset = Paths.get(pathDatasetRoot, cut + "_test.csv");

            System.out.println(pathDataset);

            List<LinkedList<InputTriple>> dataset = DatasetLoader.loadFromCSV(pathDataset.toString());


            Set<Integer> numCPH = new HashSet<>();
            numCPH.add(1);

            for (int numInCPH : numCPH) {

                System.out.println("CPH " + numInCPH);

                ModelFitterFixedEmissions bestMf = null;
                ModelFitterFixedEmissions mf;

                for (int i = 0; i < MAX_OPS; i++) {
                    System.out.println("### Fitting Model " + i);
                    mf = new ModelFitterFixedEmissions(macrostatesNumber, numInCPH, dataset, computeUniformization(dataset, dampeningFactor));

                    System.out.println("=== Fitted parameters ===\n" +
                            "D0:\n" + mf.getD0() +
                            "D1:\n" + mf.getD1() +
                            "M:\n" + mf.getM());

                    if (outputPath != null) {
                        Path D0OutputPath = outputPath.resolve("cut_" + cut + "_D0");
                        Path D1OutputPath = outputPath.resolve("cut_" + cut + "_D1");
                        Path MOutputPath = outputPath.resolve("cut_" + cut + "_M");
                        File infoFile = outputPath.resolve("cut_" + cut + "_info.yml").toFile();

                        mf.getD0().saveToFileCSV(D0OutputPath.toString());
                        mf.getD1().saveToFileCSV(D1OutputPath.toString());
                        mf.getM().saveToFileCSV(MOutputPath.toString());

                        try (PrintWriter out = new PrintWriter(infoFile)) {
                            out.println("LL: " + mf.getLogFitness());
                            out.println("unif: " + mf.getUniformization());
                            out.println("data_unif: " + computeUniformization(dataset, 1.0));
                            out.println("macrostates: " + macrostatesNumber);
                            out.println("dataset: " + pathDatasetRoot);
                            out.println("cut: " + cut);
                            out.println("total_cuts: " + CUTS);
                        }
                    }

                    if (sequencesToSimulate > 0) {
                        System.out.println("=== Simulating " + sequencesToSimulate + " sequences ===");
                        File sequencesFile = outputPath.resolve("sequences" + "_cut_" + cut + ".csv").toFile();
                        double[] initialStateDistribution = new double[mf.getTotalStatesNumber()];
                        initialStateDistribution[0] = 1.0;

                        ModelSimulator simulator = new ModelSimulator(
                                mf.getD0(),
                                mf.getD1(),
                                mf.getM(),
                                initialStateDistribution,
                                mf.getListEvents()
                        );

                        try (PrintWriter out = new PrintWriter(sequencesFile)) {
                            int outputSequences = 0;
                            while(outputSequences < sequencesToSimulate) {
                                List<InputTriple> sequence = simulator.simulateRun();

                                StringJoiner joiner = new StringJoiner(",");
                                double timeCounter = 0;

                                for (InputTriple t : sequence) {
                                    timeCounter += t.getTime();
                                    joiner
                                            .add(Integer.toString((int) timeCounter))
                                            .add(t.getIntensity().toString())
                                            .add(t.getType());
                                }

                                if (joiner.length() > 0) {
                                    out.println(joiner);
                                    outputSequences++;
                                }
                            }
                        }
                    }

					if (bestMf == null) {
						bestMf = mf;
					}

                    if (bestMf.getLogFitness() < mf.getLogFitness()) {
                        bestMf = mf;
                    }
                }
                //bestMf.print(pathSaveModelType);
                System.out.println("=== Fitted " + pathDatasetRoot + " (" + (cut + 1) + "/" + CUTS + ") with " + MAX_OPS + " random restarts with LL " + bestMf.getLogFitness() + " ===");
            }
        }

        Instant endTime = Instant.now();
        String duration = Duration.between(startTime, endTime)
                .toString()
                .substring(2)
                .replaceAll("(\\d[HMS])(?!$)", "$1 ")
                .toLowerCase();
        System.out.println("Fitting ended at " + endTime + ". Took " + duration);
    }


    public static void validate(String saveMF, double leftBound, double rightBound, LinkedList<LinkedList<InputTriple>> testset, LinkedList<Double> times, ModelFitter mf) {

        File file = new File(saveMF);
        try (PrintWriter writer = new PrintWriter(file)) {

            int listIndex = -1;

            for (LinkedList<InputTriple> list : testset) {
                listIndex++;
                //				System.out.println("validating "+listIndex);


                //for prediction truth and to stop before end (before end-leftBound)
                double timeToEnd = times.get(listIndex);
                double runTime = 0.;

                boolean canDoCurrentStep = (timeToEnd - runTime > leftBound);

                while (canDoCurrentStep) {

                    LinkedList<InputTriple> currentList = new LinkedList<InputTriple>();

                    double remainingTime = runTime;

                    for (InputTriple p : list) {
                        double needed = p.getTime();
                        if (needed > remainingTime || p.getType().equals("END")) {
                            break;
                        }
                        currentList.add(p);
                        remainingTime -= needed;
                    }

                    if (remainingTime < 0.0) {
                        System.out.println("ERROR REMAINING < 0");
                        break;
                    }

                    if (remainingTime > 0.0) {
                        currentList.add(new InputTriple(remainingTime, 0., "NO_EVENT"));
                    }

                    Double[] diagn = ModelValidator.ForwardProbabilities(currentList, mf, true);

                    //riporto a 1 (elimino perdite da floating point) ed elimino prob da ultimo stato caso ce ne fosse
                    Double psum = 0.;
                    for (int i = 0; i < diagn.length - 1; i++) {
                        psum += diagn[i];
                    }

                    for (int i = 0; i < diagn.length - 1; i++) {
                        diagn[i] = diagn[i] / (psum);
                    }
                    diagn[diagn.length - 1] = 0.;

                    ModelValidator mv = ModelValidator.integrateFromModel(leftBound, rightBound, mf, diagn);

                    //FIXME
                    int truth = ((timeToEnd - runTime) > leftBound && (timeToEnd - runTime) < rightBound) ? 1 : 0;

                    double value = mv.getIntegral();
                    writer.write(value + "," + truth + ",");

                    runTime += STEP;

                    canDoCurrentStep = (timeToEnd - runTime > leftBound);
                }
                writer.println();

            }

        } catch (FileNotFoundException e) {
            System.out.println("errore");
            System.out.println(e.getMessage());
        }

    }


    public static LinkedList<InputTriple> cloneList(LinkedList<InputTriple> list) {
        LinkedList<InputTriple> l = new LinkedList<InputTriple>();
        for (InputTriple p : list) {
            l.add(p);
        }
        return l;
    }

    @SuppressWarnings("deprecation")
    public static void getTest(String csvFile, LinkedList<LinkedList<InputTriple>> testset, LinkedList<Double> times) {

        String line = "";

        int count = -1;

        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {

            //&& count<201
            while ((line = br.readLine()) != null) {
                count++;
                String[] pieces = line.split(",");

                if (count % 2 == 0) {
                    LinkedList<InputTriple> list = new LinkedList<InputTriple>();

                    for (int i = 0; i < pieces.length; i = i + 3) {
						if (i + 2 > pieces.length - 1) {
							System.out.println("heyy");
						}
                        InputTriple p = new InputTriple(new Double(pieces[i]), new Double(pieces[i + 1]), pieces[i + 2]);
                        list.add(p);
                    }
                    testset.add(list);


                } else {
                    Double ptimes = new Double(pieces[0]);
                    times.add(ptimes);
                }

            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public static String fmt(double d) {
		if (d == (int) d) {
			return String.format("%5d", (int) d);
		} else {
			return String.format("%.5s", d);
		}
    }

    private static double computeUniformization(List<LinkedList<InputTriple>> dataset) {
        // heuristic: in order not to be too skewed towards modeling tiny interevent times we apply a "dampening" factor to the computed uniformization constant
        double dampeningFactor = 1. / 25;

        return computeUniformization(dataset, dampeningFactor);
    }

    private static double computeUniformization(List<LinkedList<InputTriple>> dataset, double dampeningFactor) {
        // TODO: Compute `minInterevent` directly instead of storing sequence from where it originated
        Pair<LinkedList<InputTriple>, Double> minData = Streams.mapWithIndex(dataset.stream(),
                        (record, idx) -> new Pair<>(record, record.stream().map(i -> i.getTime()).mapToDouble(Double::doubleValue).min().getAsDouble()))
                .min(Comparator.comparingDouble(Pair::getSecond))
                .get();

        double minInterevent = minData.getSecond();
        assert minInterevent > 0;
        System.out.println("minData: " + minData);

        return (1. / minInterevent) * dampeningFactor;
    }

}
