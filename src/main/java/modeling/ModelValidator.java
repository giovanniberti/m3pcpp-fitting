package modeling;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.math3.random.RandomDataGenerator;
import org.apache.commons.math3.util.Pair;
import org.ejml.simple.SimpleMatrix;

public class ModelValidator {

	private static int RUNS = 20000;


	private double integral;
	private double[] samples;

	public ModelValidator(double[] samples, double integral) {
		this.integral=integral;
		this.samples=samples;
	}


	public double[] getSamples() {
		return samples;
	}

	/**
	 * Returns the specific integral calculated in the integrateFromModel() function
	 * 
	 */
	public double getIntegral() {
		return integral;
	}


	/**
	 * Produces the states probabilities in an Hard constrained way through Viterbi Algorithm
	 * Following the approach of Liu,Rehg "Efficient Learning of Continuous-Time Hidden Markov Models for Disease Progression 
	 * 
	 * <p>Should be faster than the ForwardProbabilities approach
	 * 
	 * <p>Each step multiplies the prob. of being in a state (T1[]) for the Ak->j prob. of changing state * P1(j,j)
	 * 
	 * @param sequence is the sequence of pairs: (timeFromLastEvent, typeOfEvent), the last pair <i>may</i> have "NO_EVENT" has a type, 
	 * in which case the last time corresponds to time passed without seeing any event
	 * @param model is the fitted MMPP model, can present macrostates composed of CPHs
	 */
	//	public static Double[] ViterbiProbabilities(LinkedList<InputTriple> sequence, ModelFitter model) {
	//		SimpleMatrix M = model.getM();
	//		SimpleMatrix P1 = model.getP1();
	//
	//		List<String> listEvents = model.getListEvents();
	//		int[] macrostates = model.getMacrostates();
	//
	//		int events = sequence.size();
	//		Double emptyTime= null;
	//		if(sequence.getLast().getSecond()=="NO_EVENT") {
	//			events--;
	//			emptyTime = sequence.getLast().getFirst();
	//		}
	//
	//
	//		int statesNum = model.getStatesNumber();
	//
	//		//in zero init
	//		Double[][] T1= new Double[events+1][statesNum];
	//
	//		for(int i=0; i<statesNum;i++) {
	//			T1[0][i]= (i==0)? Double.ONE : 0.;
	//		}
	//
	//		for(int obs=1; obs<events+1;obs++) {
	//
	//			// si vuole precedente distribuzione(x) * D0[ti](x,y)*P1(y)
	//			SimpleMatrix ProbMatrix = model.unifP0(sequence.get(obs-1).getFirst()).multiply(P1);
	//
	//			for(int st=0; st<statesNum;st++) {
	//
	//				Double max=0.;
	//				for(int m=0; m<statesNum;m++) {
	//
	//					Double Am_st= ProbMatrix.get(m, st);
	//
	//					Double curr= T1[obs-1][m].multiply(M.get(macrostates[st], listEvents.indexOf(sequence.get(obs-1).getSecond()))).multiply(Am_st);
	//
	//					max = (max.compareTo(curr)>0) ? max : curr;
	//
	//				}
	//
	//				T1[obs][st]= max;
	//			}
	//
	//		}
	//
	//
	//
	//
	//		Double[] X = T1[events].clone();
	//
	//
	//		if(emptyTime!=null) {
	//			SimpleMatrix v = new Array2DRowSimpleMatrix(DoubleField.getInstance(),X).transpose().multiply(model.unifP0(emptyTime));
	//			X = v.getRow(0);
	//		}
	//
	//		Double counter=0.;
	//		for(int k=0; k<model.getStatesNumber();k++){
	//			counter= counter+(X[k]);
	//		}
	//
	//		if(counter==0.){
	//			System.out.println("Probability sums to 0, validatore Viterbi");
	//		}
	//
	//
	//		for(int i=0; i<X.length;i++) {
	//			X[i]= X[i].divide(counter);
	//		}
	//
	//
	//		return X;
	//	}



	/**
	 * Returns the distribution of probability of having reached a particular state in the model
	 * sums to 1
	 * 
	 * attenzione ci pu� essere probabilit� non nulla che ci si trovi in fallito!!!
	 * 
	 * @param sequence is the sequence of pairs: (timeFromLastEvent, typeOfEvent), the last pair <i>may</i> have "NO_EVENT" has a type, 
	 * in which case the last time corresponds to time passed without seeing any event
	 * @param model is the fitted MMPP model, can present macrostates composed of CPHs
	 */
	//sto creando una matrice di likelihood di cui per� ogni volta mi serve solo il round attuale...
	//lo moltiplico per la matrice P0 e P1, per M e siamo a next round, la matrice grossa non mi serve.. ma allo stesso tempo sto solo salvando i dati... non fo calcoli aggiuntivi..
	public static Double[] ForwardProbabilities(LinkedList<InputTriple> sequence, ModelFitter model) {
		return ForwardProbabilities(sequence, model, false);
	}	


	/**
	 * Forward Probabilities starting from a random point:
	 * 
	 * used noninformative prior corresponding to the mean time passed in each state, (ending state is not considered)
	 * 
	 */
	public static Double[] ForwardProbabilities(LinkedList<InputTriple> sequence, ModelFitter model, boolean randomStart) {
		SimpleMatrix M = model.getM();
		SimpleMatrix P1 = model.getP1espanso();

		List<String> listEvents = model.getListEvents();


		int seq_size= sequence.size();
		Double emptyTime= null;

		if(sequence.size()==0) {
			double[] arr = model.getProbOfBeingInState();
			Double[] sol= new Double[arr.length];
			for(int i=0;i<arr.length;i++) {
				sol[i]= arr[i];
			}

			return sol;
		}


		if(sequence.getLast().getType()=="NO_EVENT") {
			seq_size--;
			emptyTime = sequence.getLast().getTime();
		}

		SimpleMatrix forwardLikelihood = new SimpleMatrix(seq_size+1,model.getTotalStatesNumber());



		//		if(randomStart){
		//			double[] av =model.getProbOfBeingInState();
		//			for(int i=0; i<model.getTotalStatesNumber();i++){
		//				forwardLikelihood.set(0, i, av[i]);
		//			}
		//		}else {
		//			forwardLikelihood.set(0, 0, 1.);
		//		}

		forwardLikelihood.set(0, 0, 1.);

		//shifto di 1 perch� forwardLikelihood parte da 0
		for(int j=1; j<seq_size+1; j++){

			SimpleMatrix newRow;
			if(listEvents.contains(sequence.get(j-1).getType())) {

				newRow = forwardLikelihood.rows(j-1,j).mult(model.unifP0loweredExponent(sequence.get(j-1).getTime())).mult(P1);


				for(int m=0;m<model.getTotalStatesNumber();m++){

					//				System.out.println(m);
					//				for(int i=0;i<listEvents.size();i++){
					//					System.out.print(listEvents.get(i)+" ");
					//				}
					//				System.out.println();
					//				System.out.println(sequence.get(j-1).getType());
					//					

					newRow.set(0, m, newRow.get(0, m)* M.get(model.getIndexOfCphForState()[m], listEvents.indexOf(sequence.get(j-1).getType())));
				}
			}else {
				newRow = forwardLikelihood.rows(j-1,j).mult(model.unifP0normalExponent(sequence.get(j-1).getTime()));

			}


			Double counter=0.;
			for(int k=0; k<model.getTotalStatesNumber();k++){
				counter= counter+(newRow.get(0, k));
			}

			if(counter==0.){
				System.out.println("Forward Probability sums to 0, validatore");
			}
			forwardLikelihood.insertIntoThis(j,0, newRow.divide(counter));

		}


		Double[] sol=new Double[forwardLikelihood.numCols()];
		if(emptyTime==null) {

			for(int i=0;i<forwardLikelihood.numCols();i++) {
				sol[i]= forwardLikelihood.get(seq_size,i);
			}

		}else {
			SimpleMatrix res =  forwardLikelihood.rows(seq_size,seq_size+1).mult(model.unifP0normalExponent(emptyTime));

			for(int i=0;i<forwardLikelihood.numCols();i++) {
				sol[i]= res.get(0,i);
			}

		}

		return sol;
	}	




	/**
	 *
	 * Integrates a distribution corresponding to the states diagnosis in a particular model
	 *  >leftBound <=rightBound
	 * (means that if I'm already failed and leftB=0 it's not summed
	 * 
	 * only triangular models!
	 * !!!Giving probability nonzero to the last state means the system is already failed..so I cannot predict a new failing
	 * 
	 * It's a good idea to re-normalize eliminating the probability of being failed, since in that case you have already "lost"
	 * 
	 * @param stateDistribution is the distribution over states of the model, sums to 1
	 */
	public static ModelValidator integrateFromModel(double leftBound, double rightBound, ModelFitter model, Double[] stateDistribution){

		ModelValidator newModel=null;
		try {
			int statesN = model.getTotalStatesNumber();
			SimpleMatrix D0= model.getD0();

			double[] stateLambdas = new double[statesN];
			for(int i=0; i<statesN;i++){
				for(int j=i+1; j<statesN;j++){
					stateLambdas[i]+= D0.get(i, j);
				}
			}

			double[][] transitionProbabilities= new double[statesN][statesN]; 

			//SOLO PER TRIANGOLARI
			//non uso i=statesN poich� divide per 0
			for(int i=0; i<statesN-1;i++){
				for(int j=i+1; j<statesN;j++){
					transitionProbabilities[i][j]=D0.get(i, j)/stateLambdas[i];
				}
			}



			double[] samples= new double[RUNS];
			double integral=0.0;

			RandomDataGenerator rng=new RandomDataGenerator();


			for (int i=0;i<RUNS;i++) {
				double elementSum=0.0;


				//index over states, represent the state under simulation at the moment, changes over the path
				int currentState=0;
				double rand= Math.random();
				double chooseState=stateDistribution[0];
				while(rand>chooseState) {
					currentState++;
					chooseState+= stateDistribution[currentState];

					if(currentState==statesN ) {
						System.out.println("Error index arrivato allo stato terminante");
					}

				}


				while(currentState!=statesN-1) {


					elementSum +=rng.nextExponential(1/stateLambdas[currentState]);
					rand= Math.random();
					chooseState=0.0;
					int next= currentState;
					//parto da zero prob che next==current
					while(rand>chooseState) {
						next++;

						if(currentState==statesN-1 || next==statesN ) {
							System.out.println("Error index");
						}

						chooseState+= transitionProbabilities[currentState][next];
					}
					currentState=next;

				}
				samples[i]= elementSum;

				if(elementSum > leftBound && elementSum <= rightBound) {
					integral++;
				}
			}
			integral= integral/RUNS;

			newModel = new ModelValidator(samples,integral);

		}catch(Exception e) {
			int i=0;
		}

		return newModel;
	}

}
