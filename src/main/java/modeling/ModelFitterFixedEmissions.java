package modeling;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedList;

import java.util.List;

import org.apache.commons.math3.analysis.function.Gaussian;
import org.apache.commons.math3.exception.NotPositiveException;
import org.apache.commons.math3.linear.NonSquareMatrixException;
import org.ejml.simple.SimpleMatrix;
import org.oristool.models.gspn.chains.FoxGlynn;

public class ModelFitterFixedEmissions implements ModelFitter{


	private double[] probOfBeingInState;

	//ogni tipo di evento � associato ad una gaussiana (o esponenziale o mixture di gaussiane)
	//tratto solo il caso di gaussiana singola non multivariata?

	private double[] initialProbabilities;


	//(no multivariata per il momento, dovrei usare vettori di SimpleMatrix nel caso)
	//(no mixture al momento)
	/**
	 * vettori di medie di ogni gaussiana [evento][macrostato]
	 */	
	private double[][] gaussianMeans;

	/**
	 * vettori di varianze di ogni gaussiana [evento][macrostato]
	 */	
	private double[][] gaussianVariances;

	//se avessi mixture per ogni tipo di evento dovrei avere distribuzione delle gaussiane 
	//private double[] probMixture; 
	//private int mixtureNumber;

	// for each macrostate a vector of probabilities of the inner states 
	private double[][] initialProbCPH;

	private int totalStatesNumber;
	private int statesPerCphNumber;
	private int eventTypes;

	private int[] indexOfCphForState;
	private int macrostatesNumber; 


	private LinkedList<String> listEvents;
	//	private LinkedList<String> listAct;
	//contiene alla posizione dello stato una data activity
	//	private LinkedList<String> actForState;
	//	private Map<String,Set<Integer>> mapActState;



	private List<LinkedList<InputTriple>> dataset;
	private SimpleMatrix M;
	private SimpleMatrix D0;
	private SimpleMatrix D1macro;

	private SimpleMatrix P0;
	private SimpleMatrix P1macro;
	private SimpleMatrix P1espanso;

	private Double uniformization;


	private SimpleMatrix initialM;
	private SimpleMatrix initialP0;
	private SimpleMatrix initialP1macro;

	/**
	 * Forward likelihood:   matrice [traccia] quindi [evento][stato]
	 */
	private SimpleMatrix[] forwardArray;
	private SimpleMatrix V;
	/**
	 * Backward likelihood:  matrice [traccia] quindi [stato][evento]
	 */
	private SimpleMatrix[] backwardArray;
	private SimpleMatrix W;


	private SimpleMatrix[] powersP0;

	private double logfitness;
	private int countRuns;

	private static final double UNIF_ERR= 0.001;  // %
	private static final double OUTER_UNIF_ERR= 0.001;


	private static final int MAX_RUNS = 100;
	private static final double MIN_LOGLIKELIHOOD_RELATIVE_DIFFERENCE = 0.001;

	private static final int MAX_EVENTS_PER_SEQUENCE=1000;


	/**
	 * Fit a new M3PCPP model with the supplied number of states and dataset. 
	 * Eventually utilizes as starting points of the fitting the supplied constraint matrixes
	 *
	 * Attention! Does not perform checks on passed Matrixes validity!
	 *
	 * @param statesNumber Number of total states of the MMPP model, states can be composed into CPHs structures
	 * @param subStates array containing the structure of the (eventual) CPHs i.e. [3,2,3] corresponds to statesNumber=8, composed into 3 CPHs if NULL it is instantiated by default as no CPHs present
	 * @param dataset Set of time-stamped and typed sequences of events.
	 * @param M matrix of events probabilities per state, can be used to suppress events in certain states.
	 * @param D0 matrix of unobserved events, can be used to constrain the available transitions between states, left to right
	 * @param D1 matrix of observed events, must be diagonal 
	 * @throws NotStrictlyPositiveException if the row or column dimension is
	 * not positive.
	 */
	public ModelFitterFixedEmissions(int macrostatesNumber_in, int subStatesNumber_in, List<LinkedList<InputTriple>> dataset_in, Double unif) {

		//this.mixtureNumber = mixtureNumber_in;


		//se c'� un numero di substates allora ogni macrostato ha quel numero di substates
		this.statesPerCphNumber = (subStatesNumber_in>0)? subStatesNumber_in : 1;

		this.macrostatesNumber = macrostatesNumber_in;
		this.totalStatesNumber=macrostatesNumber * statesPerCphNumber;

		List<LinkedList<InputTriple>> data = dataset_in; //dataset_in .subList(1, 2)


		this.dataset=data;

		this.logfitness= Double.NEGATIVE_INFINITY;

		this.listEvents = new LinkedList<>();

		// < del numero di macrostati
		//		this.listAct = new LinkedList<>();


		for(LinkedList<InputTriple> list : dataset) {
			for(InputTriple triple : list) {
				if(!listEvents.contains(triple.getType())) {
					listEvents.add(triple.getType());
				}
				//				if(!triple.getAct().equals("empty")&&!listAct.contains(triple.getAct())) {
				//					listAct.add(triple.getAct());
				//				}
			}		
		}



		//		if(listAct.size()>macrostatesNumber) {
		//			System.out.println("ERRORE: Pi� activity che stati");
		//			return;
		//		}


		//		mapActState = new HashMap<String,Set<Integer>>();
		//		for(String s: listAct) {
		//			mapActState.put(s, new HashSet<Integer>());
		//		}
		//		if(!mapActState.containsKey("toilet"))
		//			mapActState.put("toilet", new HashSet<Integer>());
		//
		//		if(actForS!=null) {
		//			actForState=actForS;
		//			if(actForState.size()!=macrostatesNumber)
		//				System.out.println("ERRORE PASSATE ACTIVITY PER STATO MA NUMERO DIVERSO "+actForState.size()+" "+macrostatesNumber);
		//		}else {
		//			int count=0;
		//			actForState = new LinkedList<>();
		//			for(int i=0;i<listAct.size();i++) {
		//				for(int j=0;j<multiplierMacro;j++){
		//					mapActState.get(listAct.get(i)).add(count);
		//					count++;
		//					actForState.add(listAct.get(i));
		//				}
		//			}
		//			for(int i=listAct.size();i<macrostatesNumber_in;i++) {
		//				for(int j=0;j<multiplierMacro;j++) {
		//					System.out.println("activity sono meno dei macrostati, aggiunto extra stato toilet");
		//					actForState.add("toilet");
		//					mapActState.get("toilet").add(count);
		//					count++;
		//				}
		//			}
		//
		//		}

		//		if(actForS!=null) {
		//			System.out.println("NON IMPLEMENTATO MAPPA");
		//			return;
		//		}

		double[][] initmeans=new double[listEvents.size()][macrostatesNumber];
		double[][] initvar=new double[listEvents.size()][macrostatesNumber];

		double[][] initialEventProbabilities = new double[macrostatesNumber][listEvents.size()];
		int[] countMacro = new int[macrostatesNumber];

		//		for(LinkedList<InputTriple> list : dataset) {
		//			for(InputTriple triple : list) {
		//				for(Integer i: mapActState.get(triple.getAct())) {
		//					initialEventProbabilities[i][listEvents.indexOf(triple.getType())]+=1;
		//					initmeans[listEvents.indexOf(triple.getType())][i]+=triple.getIntensity();
		//					initvar[listEvents.indexOf(triple.getType())][i]+=triple.getIntensity()*triple.getIntensity();
		//					countMacro[i]++;
		//				}
		//			}		
		//		}

		//modificato prima assegnavo ad ogni stato la sua distribuzione
		for(LinkedList<InputTriple> list : dataset) {
			for(InputTriple triple : list) {
				if(triple.getType().equals("end")) {
					initialEventProbabilities[macrostatesNumber-1][listEvents.indexOf(triple.getType())]+=1;
					initmeans[listEvents.indexOf(triple.getType())][macrostatesNumber-1]+=triple.getIntensity();
					initvar[listEvents.indexOf(triple.getType())][macrostatesNumber-1]+=triple.getIntensity()*triple.getIntensity();
					countMacro[macrostatesNumber-1]++;
					
				}else {
					for(int i=0;i<macrostatesNumber-1;i++) {
						initialEventProbabilities[i][listEvents.indexOf(triple.getType())]+=1;
						initmeans[listEvents.indexOf(triple.getType())][i]+=triple.getIntensity();
						initvar[listEvents.indexOf(triple.getType())][i]+=triple.getIntensity()*triple.getIntensity();
						countMacro[i]++;
					}
				}
			}		
		}


		this.initialProbCPH= new double[macrostatesNumber][statesPerCphNumber];

		this.indexOfCphForState=new int[totalStatesNumber];


		this.eventTypes=listEvents.size();



		for(int i=0;i<eventTypes;i++) {
			for(int j=0;j<macrostatesNumber;j++) {
				//sfrutto il fatto che per ora in 
				initmeans[i][j]/= initialEventProbabilities[j][i];
				initvar[i][j]= initvar[i][j]/initialEventProbabilities[j][i] -initmeans[i][j]*initmeans[i][j];

				initialEventProbabilities[j][i] /= countMacro[j];
			}
		}



		//initialized as average of dataset
		this.gaussianMeans = new double[eventTypes][macrostatesNumber];
		this.gaussianVariances = new double[eventTypes][macrostatesNumber];

		for(int i=0;i<eventTypes;i++) {
			for(int j=0; j<macrostatesNumber;j++) {
				gaussianMeans[i][j]=initmeans[i][j];
				gaussianVariances[i][j]=initvar[i][j];
			}
		}



		int counterForDecidingCPH=0;
		for(int j=0; j<macrostatesNumber;j++) {
			for(int i=0; i<statesPerCphNumber; i++) {
				this.indexOfCphForState[counterForDecidingCPH]=j;
				counterForDecidingCPH++;
			}
		}


		//initial probability is currently uniform (could be random)
		//TODO make the initial probabilities dependent on the average time in state,
		//..but depend on dataset so do the same for the states
		this.initialProbabilities= new double[totalStatesNumber];

		initialProbabilities[0]=1;

		//		for(int i=0; i<totalStatesNumber; i++) {
		//			initialProbabilities[i]= 1./totalStatesNumber;
		//		}




		//mixture probability is currently uniform (could be random)
		//this.probMixture = new double[mixtureNumber];
		//for(int i=0; i<mixtureNumber; i++) {
		//	probMixture[i]= 1./mixtureNumber;
		//}


		//M is initialized to the probability of each event type obtained from the frequency
		this.M = new SimpleMatrix(macrostatesNumber, eventTypes);
		for(int i=0; i<macrostatesNumber; i++) {
			for(int j=0; j<eventTypes; j++) {
				//initialized by background
				//FIXME
				M.set(i, j, initialEventProbabilities[i][j]);
				//M.set(i, j, 1./eventTypes);
			}
		}



		//if uniform was desired //		M = M.scalarAdd(new Double(eventTypes).reciprocal());


		//alfa � preso inverso del minimo tempo della traccia
		//alfa � COSTANTE, dipendente dall'intero dataset
		uniformization = Double.MAX_VALUE;
		for(LinkedList<InputTriple> list: dataset) {
			for(InputTriple triple : list) {
				if(uniformization.compareTo(triple.getTime())>0)
					uniformization = triple.getTime();
			}
		}
		uniformization= 1/uniformization;
		System.out.println("Uniformization given dataset should be: "+ uniformization);

		if(unif != null) {
			System.out.println("Chosen value: "+unif);
			uniformization = unif;
		}


		RandomizeD();


		fit();

	}




	/**
	 * Generates D0 and D1 randomly but such that the mean sojourn time in the MMPP
	 *  is equal to the mean time to completion in the dataset
	 *  
	 *  D0 is triangular superior, D1 is a diagonal matrix
	 */		
	private void RandomizeD() {
		D0 = new SimpleMatrix(totalStatesNumber,totalStatesNumber);

		D1macro = new SimpleMatrix(macrostatesNumber, macrostatesNumber);


		Double meanEventTime = 0.;
		Double meanEndTime=0.;

		if(dataset.size()==0) {
			System.out.println("dataset has size 0");
			return;
		}

		int count=0;
		int countEnd=0;
		for(LinkedList<InputTriple> list : dataset) {
			for(InputTriple triple : list) {
				if(triple.getType().equals("end")) {
					countEnd++;
					meanEndTime= meanEndTime+triple.getTime();
				}else {
					count++;
					meanEventTime= meanEventTime+triple.getTime();
				}
			}
		}
		meanEventTime = meanEventTime/count;
		meanEndTime = meanEndTime/countEnd;

		double meanRate = 1./meanEventTime;
		double meanEndRate=1./meanEndTime;


		//init probCPH random
		for(int i=0; i<macrostatesNumber; i++) {
			double sum=0.;
			for(int j=0; j<statesPerCphNumber; j++) {
				double value= Math.random();
				initialProbCPH[i][j]=value;
				sum+=value;
			}
			for(int j=0; j<statesPerCphNumber; j++) {
				initialProbCPH[i][j]/=sum;
			}
		}	



		for (int i=0; i<totalStatesNumber; i++) {
			for (int j=i+1; j<totalStatesNumber && indexOfCphForState[j]==indexOfCphForState[i]; j++) {

				double value= Math.random();
				while(value<0.1 || value>0.9) {
					value=Math.random();
				}

				D0.set(i, j, D0.get(i, j)+value);
				D0.set(i, i, D0.get(i, i)-value);

			}
		}

		for (int i=0; i<macrostatesNumber; i++) {
			for (int j=i+1; j<macrostatesNumber; j++) {

				double value= Math.random();
				while(value<0.1 || value>0.9) {
					value=Math.random();
				}

				D0.set((i+1)*statesPerCphNumber-1, j*statesPerCphNumber, D0.get((i+1)*statesPerCphNumber-1, j*statesPerCphNumber)+value);
				D0.set((i+1)*statesPerCphNumber-1, (i+1)*statesPerCphNumber-1, D0.get((i+1)*statesPerCphNumber-1, (i+1)*statesPerCphNumber-1)-value);

			}
		}

		//dividi secondo initProbCph i valori su Y0
		for(int i=0; i<D0.numRows();i++) {
			for(int j=0; j<macrostatesNumber;j++) {
				if(indexOfCphForState[i]!= j) {
					double toDivide = D0.get(i, j*statesPerCphNumber);
					if(toDivide>0.) {
						for(int k=0; k<statesPerCphNumber;k++) {
							D0.set(i, j*statesPerCphNumber+k, toDivide*initialProbCPH[j][k]);
						}
					}
				}
			}
		}


		Double meanTraceTime = 0.;

		if(dataset.size()==0) {
			System.out.println("dataset has size 0");
			return;
		}

		for(LinkedList<InputTriple> list : dataset) {
			for(InputTriple pair : list) {
				meanTraceTime= meanTraceTime+pair.getTime();

			}
		}
		meanTraceTime = meanTraceTime/dataset.size();



		double[] sojournRates = new double[totalStatesNumber];

		//sojournRate in last state is 0 since there's no exit
		for(int i=0; i<totalStatesNumber-1;i++){
			for(int j=i+1; j<totalStatesNumber;j++){
				sojournRates[i]+= D0.get(i, j);
			}
		}

		double[][] prob= new double[totalStatesNumber][totalStatesNumber]; 

		for(int i=0; i<totalStatesNumber-1;i++){
			for(int j=i+1; j<totalStatesNumber;j++){
				prob[i][j]=D0.get(i, j)/sojournRates[i];
			}
		}

		double[] probVisitingState = new double[totalStatesNumber];
		Double averageRunLenght =  0.;

		//inizializza
		probVisitingState[0] = 1;
		for(int i=0; i<totalStatesNumber-1;i++){

			//			System.out.println(sojournRates[i]);
			//			System.out.println(i);

			//tempo medio � inverso rate
			averageRunLenght = averageRunLenght+(probVisitingState[i]/sojournRates[i]);	
			//spalma sui futuri, ma non usa mai last
			for(int j=i+1; j<totalStatesNumber;j++){
				if(prob[i][j]!= 0)
					probVisitingState[j]+= probVisitingState[i]*prob[i][j];
			}
		}

		//dell'ultimo stato si modifica il rate ma nient'altro 
		D0 = D0.scale(averageRunLenght/meanTraceTime);

		for (int i=0; i<totalStatesNumber-statesPerCphNumber; i++) {
			D0.set(i, i, D0.get(i, i)-meanRate);
		}
		
		for (int i=0; i<macrostatesNumber-1; i++) {
			D1macro.set(i, i, D1macro.get(i, i)+meanRate);
		}
		
		D1macro.set(macrostatesNumber-1, macrostatesNumber-1, D1macro.get(macrostatesNumber-1, macrostatesNumber-1)+meanEndRate);


		for (int i=totalStatesNumber-statesPerCphNumber; i<totalStatesNumber; i++) {
			D0.set(i, i, D0.get(i, i)-meanEndRate);
		}

		
	}

	private void fit() {

		System.out.println("Dentro fit");
		int runCounter =0;
		initialM=M;

		int traceNumber = dataset.size();

		//non specificati generics, darebbe warning
		forwardArray= new SimpleMatrix[traceNumber];
		backwardArray= new SimpleMatrix[traceNumber];




		//qui si ha ancora inverso di alfa
		P1macro = D1macro.scale(1/uniformization);
		P0 = D0.scale(1/uniformization);
		for(int i=0; i<totalStatesNumber;i++) {
			P0.set(i, i, P0.get(i, i)+ 1.);
		}

		P1espanso = new SimpleMatrix(totalStatesNumber,totalStatesNumber);
		for(int i=0;i<totalStatesNumber;i++) {
			double value= P1macro.get(indexOfCphForState[i], indexOfCphForState[i]);

			P1espanso.set(i, i, value);

		}

		initialP0 = P0;
		initialP1macro=P1macro;

		System.out.println(P1macro.toString());
		System.out.println(P1espanso.toString());

		double logLikelihoodDiff;

		SimpleMatrix Y0;
		SimpleMatrix Y1=null;
		SimpleMatrix Z=null;

		//		double[]   initProbsTemp;
		double[][] gaussianMeansTemp;
		double[][] gaussianVariancesTemp;
		double[][] initProbCPHTemp;




		double logrunFitness=  Double.NEGATIVE_INFINITY;

		//ciclo fitting: si ferma raggiunto num max iterazioni o se log likelihood ha modifica relativa< MIN
		do{



			//FITNESS � 0 per essere minore al primissimo round
			logfitness=logrunFitness;

			//resetto P0
			powersP0 = new SimpleMatrix[2000];

			powerP0array(powersP0);

			System.out.println("c="+runCounter);
			logrunFitness=0.;

			runCounter++;

			//accumulatori riscalati
			//REMARK In Y0 si hanno il numero medio di passaggi da i a j non osservati
			// temporaneamente, per passaggi fra differenti macrostati, si fa store nel primo sottostato associato 
			// al differente macrostato dell'intera quantit�, mentre separatamente si calcola alfa
			// al termine si deve ripartire!!!
			Y0 = new SimpleMatrix(totalStatesNumber,totalStatesNumber);
			Y1 = new SimpleMatrix(macrostatesNumber,macrostatesNumber);
			Z =  new SimpleMatrix(macrostatesNumber, eventTypes);

			//REMARK used to make sure Z is never completely empty
			//E' sensato? rischio di mettere eventi che non appartengono
			// nella realt� dividendo per traceLikelihood non dovrebbe dare problemi..
			//
			Z= Z.plus(Double.MIN_VALUE);


			//contengono immediatamente i componenti riscalati da TraceProb 
			//			initProbsTemp          = new double[totalStatesNumber]; 
			initProbCPHTemp       = new double[macrostatesNumber][statesPerCphNumber];
			
			for(int k=0;k<macrostatesNumber;k++) {
				for(int kk=0;kk<statesPerCphNumber;kk++) {
					initProbCPHTemp[k][kk]+=Double.MIN_VALUE;
				}
			}


			gaussianMeansTemp      = new double[eventTypes][macrostatesNumber];
			gaussianVariancesTemp  = new double[eventTypes][macrostatesNumber];

			double[] gaussNormalization = new double[macrostatesNumber];

			//servono per ricostruire la varianza
			//REMARK care max number events for list
			double[][][] probsIatKMap  = new double[dataset.size()][MAX_EVENTS_PER_SEQUENCE][macrostatesNumber];
			double[] probTraceList = new double[dataset.size()];


			for(int traceIndex=0; traceIndex< dataset.size();traceIndex++) {


				LinkedList<InputTriple> list = dataset.get(traceIndex);
				int traceLenght = list.size();

				forwardLconstructor(list, traceIndex);
				backwardLconstructor(list, traceIndex);


				SimpleMatrix X0 = new SimpleMatrix(totalStatesNumber,totalStatesNumber);
				SimpleMatrix X1= null;
				SimpleMatrix Ztemp=null;


				X1 = new SimpleMatrix(macrostatesNumber,macrostatesNumber);


				Ztemp =  new SimpleMatrix(macrostatesNumber, eventTypes);

				Double traceProbability= 0.;
				for(int count=0; count<totalStatesNumber;count++){
					//traceLenght e non -1 in quanto in 0 c'� (1,0,0,0..)
					traceProbability= traceProbability+forwardArray[traceIndex].get(traceLenght, count);
				}

				probTraceList[traceIndex]= traceProbability;

				//update initProbs
				//sommo le prob di essere in uno stato 
				//				for(int i=0; i<totalStatesNumber;i++) {
				//					initProbsTemp[i] += initialProbabilities[i]*backwardArray[traceIndex].get(i,0) /traceProbability;
				//				}

				logrunFitness= logrunFitness + Math.log(traceProbability); 


				for(int event=0; event< traceLenght;event++){

					InputTriple ev = list.get(event);
					int idEvent = listEvents.indexOf(ev.getType());
					//String activity = ev.getAct();
					double intensity = ev.getIntensity();

					double at = uniformization * ev.getTime();

					FoxGlynn fg= generateFoxGlynnReduced(at, OUTER_UNIF_ERR);


					int leftB = fg.leftPoint();
					int rightB= fg.rightPoint();


					SimpleMatrix X0temp = new SimpleMatrix(totalStatesNumber,totalStatesNumber);



					SimpleMatrix add;
					SimpleMatrix tempV;
					SimpleMatrix tempW;

					V = getV(event,rightB, traceIndex);
					W = getW(event,rightB, traceIndex, idEvent);


					for(int k=leftB; k<rightB+1;k++) {

						//prob. di osservare k passaggi dati lambda expected
						double prob = fg.poissonProb(k);


						if(k>1) {
							// indice � zero(evento 1) e V vuole -1 ma parte da 0(+1)  W vuole stesso (ed usa b, con (O_+1) )
							tempV = V.extractMatrix(0, V.numRows(), 0, k-1);
							tempW = W.extractMatrix(W.numRows()-k+1, W.numRows(), 0, W.numCols());

							add = tempV.mult(tempW);

							X0temp = X0temp.plus(add.scale(prob));
						}

					}






					for(int i=0; i<totalStatesNumber;i++){

						int macroI = indexOfCphForState[i];

						//stati interni alla stessa macro e stati in macro diverse hanno trattamento separato
						for(int j=0; j<totalStatesNumber;j++){
							if(macroI==indexOfCphForState[j]) {
								X0.set(i, j, X0.get(i, j)+(X0temp.get(i, j)*P0.get(i, j)));
							}else {
								double val= X0temp.get(i, j)*P0.get(i, j);
								//per ogni stato di partenza l'arrivo in (m,l) dove l � il resto della divisione
								initProbCPHTemp[indexOfCphForState[j]][j%statesPerCphNumber]+=val/traceProbability;

								int macId = indexOfCphForState[j]*statesPerCphNumber;

								//accumulo nel primo stato della macro di arrivo
								X0.set(i, macId, X0.get(i, macId)+val);

							}
						}

						//REMARK la prob. di osservare evento in uno stato � 
						//la prob. di essere in quello stato al tempo k

						//dipende da trace, 
						//quindi dividere per trace mi permette di avere la prob di essere in quello stato
						double probStateIAtTimeK= forwardArray[traceIndex].get(event+1, i)*backwardArray[traceIndex].get(i, event+1);

						probsIatKMap[traceIndex][event][macroI]+=probStateIAtTimeK;

						Ztemp.set(macroI, idEvent, Ztemp.get(macroI, idEvent)+probStateIAtTimeK );

						X1.set(macroI, macroI, X1.get(macroI, macroI)+probStateIAtTimeK);

						if(false) {
							gaussianMeansTemp[idEvent][macroI]+=probStateIAtTimeK*intensity/traceProbability;
							gaussNormalization[macroI] += probStateIAtTimeK/traceProbability;
						}
					}



					//termine per ogni evento, termine della singola traccia
				}


				Y0 = Y0.plus(X0.divide(traceProbability));  


				Y1 = Y1.plus(X1.divide(traceProbability)); 

				Z = Z.plus(Ztemp.divide(traceProbability)); 


				//termine for ogni traccia
			}


			if(false) {
				//calculate gausMeans
				for(int i=0; i<macrostatesNumber;i++){
					for(int j=0; j<eventTypes;j++){

						gaussianMeansTemp[j][i] = gaussianMeansTemp[j][i] /gaussNormalization[i] ;
					}
				}



				//calculate gausVariance
				double intens;
				int idType;
				int c=0;
				for(LinkedList<InputTriple> list : dataset) {

					int d=0;
					for(InputTriple triple : list) {

						intens = triple.getIntensity();
						idType = listEvents.indexOf(triple.getType());

						for(int i=0; i<macrostatesNumber;i++){
							gaussianVariancesTemp[idType][i] += 
									(intens-gaussianMeansTemp[idType][i])*(intens-gaussianMeansTemp[idType][i])*probsIatKMap[c][d][i] / probTraceList[c] ;
						}
						d++;
					}
					c++;
				}

				for(int i=0; i<macrostatesNumber;i++){
					for(int j=0; j<eventTypes;j++){

						gaussianVariancesTemp[j][i] = gaussianVariancesTemp[j][i] /gaussNormalization[i] ;
					}
				}

			}


			double sumCPH=0.;
			for(int s=0; s<initProbCPHTemp.length;s++){
				sumCPH = 0.;
				for(int e=0; e<initProbCPHTemp[s].length;e++){
					sumCPH = sumCPH+initProbCPHTemp[s][e];
				}

				if(!(sumCPH>0.)) {
					System.out.println("alfa somma 0");
				}

				for(int e=0; e<initProbCPHTemp[s].length;e++){
					initProbCPHTemp[s][e] /= sumCPH;
				}
			}

			initialProbCPH = initProbCPHTemp;

			//dividi secondo initProbCph i valori su Y0
			for(int i=0; i<Y0.numRows();i++) {
				for(int j=0; j<macrostatesNumber;j++) {
					if(indexOfCphForState[i]!= j) {
						double toDivide = Y0.get(i, j*statesPerCphNumber);
						if(toDivide>0.) {
							for(int k=0; k<statesPerCphNumber;k++) {
								Y0.set(i, j*statesPerCphNumber+k, toDivide*initProbCPHTemp[j][k]);
							}
						}
					}
				}
			}



			//normalize initProbs
			//			Double probsSum=0.;
			//			for(int i=0; i<totalStatesNumber;i++) {
			//				probsSum+= initProbsTemp[i];
			//			}
			//
			//			for(int i=0; i<totalStatesNumber;i++) {
			//				initProbsTemp[i]/=probsSum;
			//			}

			//FIXME FIXME FIXME
			//			initialProbabilities = initProbsTemp;


			//qui calcolo i valori reali di Y1, poi li utilizzer� per calcolare i valori reali di Y0
			double sumY1i=0.;
			for(int x=0; x<macrostatesNumber;x++){
				sumY1i = Y1.get(x, x);

				for(int cph=0; cph<statesPerCphNumber;cph++){
					int xId = x*statesPerCphNumber+cph;
					for(int y=0; y<totalStatesNumber;y++){
						sumY1i = sumY1i+(Y0.get(xId, y));
					}
				}
				if(!(sumY1i>0.)) {
					System.out.println("Y1 somma zero macrostato indice "+x);
				}
				Y1.insertIntoThis(x,0, Y1.rows(x,x+1).divide(sumY1i));

			}

			//qui calcolo i valori reali di Y0, sfruttando quelli precalcolati di Y1
			//\foreach i Y0(i,j) = Y0(i,j)/sum_k Y0(i,k)*(1-Y1(i,i))
			double sumY0;
			for(int x=0; x<Y0.numRows();x++){
				sumY0 = 0.;
				for(int y=0; y<Y0.numCols();y++){
					sumY0 = sumY0+(Y0.get(x, y));
				}
				if(!(sumY0>0.)) {
					System.out.println("Y0 somma zero indice"+x);
				}

				sumY0 = sumY0 / (1-Y1.get(indexOfCphForState[x],indexOfCphForState[x]));

				if(!(sumY0>0.)) {
					System.out.println("sumY0 � 0 dopo * (1-Y1) "+x);
				}

				Y0.insertIntoThis(x,0, Y0.rows(x,x+1).divide(sumY0));

			}



			double sumZ;
			// Z si deve normalizzare (come Y0 e Y1, solo alla fine altrimenti la perdita di informazione sulle sequenze porta a errore)
			for(int s=0; s<Z.numRows();s++){
				sumZ = 0.;
				for(int e=0; e<Z.numCols();e++){
					sumZ = sumZ+(Z.get(s, e));
				}

				if(!(sumZ>0.)) {
					System.out.println("Z somma 0");
				}

				Z.insertIntoThis(s,0, Z.rows(s,s+1).divide(sumZ));
			}

			P0 = Y0;

			P1macro = Y1;

			for(int i=0;i<totalStatesNumber;i++) {
				double value= P1macro.get(indexOfCphForState[i], indexOfCphForState[i]);

				P1espanso.set(i, i, value);

			}

			M = Z;

			if(logrunFitness < logfitness) {
				System.out.println("errore runFitness < fitness");
				return;
			}

			//DecimalFormat df = new DecimalFormat("0.####E0"); 

			System.out.println("fitnessLastCycle= "+logfitness);
			System.out.println("fitnessNew_Cycle= "+logrunFitness);

			logLikelihoodDiff = (logfitness - logrunFitness ) / logrunFitness;

			System.out.println("relative diff= "+logLikelihoodDiff);

			if(runCounter%4==0) {
				System.out.println("M");
				System.out.println(M.toString());
			}

		}while(runCounter<30 || (runCounter<MAX_RUNS && logLikelihoodDiff > MIN_LOGLIKELIHOOD_RELATIVE_DIFFERENCE));


		System.out.println("M");
		System.out.println(M.toString());

		logfitness=logrunFitness;


		System.out.println("lastRun= "+ runCounter);
		countRuns = runCounter;

		SimpleMatrix diag =new SimpleMatrix(totalStatesNumber,totalStatesNumber);
		SimpleMatrix eT = new SimpleMatrix(totalStatesNumber,1);
		eT=eT.plus(1.);
		SimpleMatrix sumM = P0.mult(eT);

		sumM= sumM.plus(P1espanso.mult(eT));
		for(int i =0; i<totalStatesNumber;i++) {
			diag.set(i, i, sumM.get(i, 0));
		}

		D0 = P0.minus(diag);
		D0= D0.scale(uniformization);

		D1macro = P1macro.scale(uniformization); 

		setProbOfBeingInState();

	}


	private void setProbOfBeingInState() {

		double sum=0.;
		double[] sojournRates = new double[totalStatesNumber];
		for(int i=0; i<totalStatesNumber-1;i++){
			for(int j=i+1; j<totalStatesNumber;j++){
				sojournRates[i]+= D0.get(i, j);
			}

		}


		double[][] prob= new double[totalStatesNumber][totalStatesNumber]; 

		for(int i=0; i<totalStatesNumber-1;i++){
			for(int j=i+1; j<totalStatesNumber;j++){
				prob[i][j]=D0.get(i, j)/sojournRates[i];
			}
		}

		double[] quantityInState= new double[totalStatesNumber];
		//inizializza, uso stessa variabile
		quantityInState[0] = 1;
		for(int i=0; i<totalStatesNumber-1;i++){
			//spalma sui futuri, non spalma sull ultimo, che esiste ma � ignorato (non rinormalizzo per quella parte)
			for(int j=i+1; j<totalStatesNumber-1;j++){
				if(prob[i][j]> 0.)
					quantityInState[j]+= quantityInState[i]*prob[i][j];
			}
			quantityInState[i] = quantityInState[i]/sojournRates[i];	
			sum+=quantityInState[i];
		}
		for(int i=0; i<totalStatesNumber-1;i++){
			quantityInState[i]=quantityInState[i]/sum ;
		}
		probOfBeingInState= quantityInState;

	}


	/**
	 * Produces norm 1 of a Matrix : the maximum of the sum of (absolute) elements in a column
	 */
	@SuppressWarnings("unused")
	private static double norm(SimpleMatrix mat){
		double currentMax = Double.MIN_VALUE;
		for(int j=0;j< mat.numCols();j++){
			double sum=0;
			for(int i=0;i< mat.numRows();i++){

				sum += Math.abs(mat.get(i, j));
			}
			currentMax = (currentMax>sum)? currentMax : sum;
		}
		if(currentMax==Double.MIN_VALUE) {
			System.out.println("norm failing: column sums to 0");
		}

		return currentMax;
	}


	//attenzione forwardLik partono da indice 0 in Buchholz ma qui usando gli array il valore 1,0,0 dovrebbe essere in -1
	// shifto tutto di un posto, SI HANNO list.size + 1 posizioni
	// i times sono tempo 0 per posizione 1
	// va da 0 a m+1 con init0 extra
	private void forwardLconstructor(LinkedList<InputTriple> list, int traceIndex) {

		forwardArray[traceIndex] = new SimpleMatrix(new double[list.size()+1][totalStatesNumber]);

		//aggiunte initial probabilities
		for(int i=0; i<totalStatesNumber; i++) {
			forwardArray[traceIndex].set(0, i, initialProbabilities[i]);
		}

		int[] eventsId = new int[list.size()];
		double[]intertimes = new double[list.size()];
		double[] intensities= new double[list.size()];

		//String[] activities = new String[list.size()];

		for(int i=0;i<list.size();i++) {
			InputTriple trip=list.get(i);
			eventsId[i]=listEvents.indexOf(trip.getType());
			intertimes[i]=trip.getTime();
			intensities[i]=trip.getIntensity();
			//activities[i]=trip.getAct();
		}



		for(int j=1; j<list.size()+1; j++){
			//			System.out.println("j="+j);
			//			System.out.println(unifP0(vals[j-1]));

			SimpleMatrix multip = unifP0loweredExponent(intertimes[j-1]);

			SimpleMatrix newRow = forwardArray[traceIndex].rows(j-1,j).mult(multip);

			//			System.out.println(newRow);

			Double counter=0.;
			for(int k=0; k<totalStatesNumber;k++){
				counter= counter+newRow.get(0, k);
			}
			if(!(counter>0.)){
				System.out.println("Forward Probability after unifP0 sums to 0");
			}

			//			newRow = newRow.divide(counter);


			newRow = newRow.mult(P1espanso);

			//			System.out.println(newRow);

			counter=0.;
			for(int k=0; k<totalStatesNumber;k++){
				counter= counter+newRow.get(0, k);
			}
			if(!(counter>0.)){
				System.out.println("Forward Probability after P1 sums to 0");
			}

			//			newRow = newRow.divide(counter);




			for(int m=0;m<totalStatesNumber;m++){

				double g =1.;

				if(false) {
					Gaussian gauss= new Gaussian(gaussianMeans[eventsId[j-1]][indexOfCphForState[m]], 
							Math.sqrt(gaussianVariances[eventsId[j-1]][indexOfCphForState[m]]));
					g = gauss.value(intensities[j-1]);

				}


				double val=M.get(indexOfCphForState[m], eventsId[j-1]);
				val= val*g;

				//					System.out.println(eventsId[j-1]+" s " +m+" "+val);


				newRow.set(0, m, newRow.get(0, m)*val );
			}



			//			System.out.println(newRow);
			//			forwardLikelihood[traceIndex].insertIntoThis(j,0, newRow);

			counter=0.;
			for(int k=0; k<totalStatesNumber;k++){
				counter= counter+newRow.get(0, k);
			}
			if(!(counter>0.)){
				System.out.println("Forward Probability after M sums to 0");
			}


			counter=0.;
			for(int k=0; k<totalStatesNumber;k++){
				counter= counter+newRow.get(0, k);
			}
			if(!(counter>0.)){
				System.out.println("Forward Probability after ACT sums to 0");
			}


			forwardArray[traceIndex].insertIntoThis(j, 0, newRow);

		}

	}

	// va da 0 a m-1 (1 a m) non da 0 in quanto DEVO avere visto almeno uno stato
	private void backwardLconstructor(LinkedList<InputTriple> list, int traceIndex) {

		backwardArray[traceIndex]= new SimpleMatrix(totalStatesNumber,list.size()+1);

		int[] eventsId = new int[list.size()];
		double[]intertimes = new double[list.size()];
		double[] intensities= new double[list.size()];


		for(int i=0;i<list.size();i++) {
			InputTriple trip=list.get(i);
			eventsId[i]=listEvents.indexOf(trip.getType());
			intertimes[i]=trip.getTime();
			intensities[i]=trip.getIntensity();

		}


		for(int i=0; i<totalStatesNumber;i++){
			backwardArray[traceIndex].set(i, list.size(), 1.);
		}

		for(int j=list.size()-1; j>=0; j--){



			SimpleMatrix newColumn =unifP0loweredExponent(intertimes[j]).mult( P1espanso);

			//			Double counter=0.;
			//			for(int k=0; k<statesNumber;k++){
			//				counter= counter+newColumn.get(k, 0);
			//			}
			//
			//			if(!(counter>0.)){
			//				System.out.println("Backward Probability sums unifP0-P1 to 0");
			//				SimpleMatrix test = unifP0(intertimes[j+1]);
			//				test.print();
			//			}






			// [a b]  M [e]  -> [ae bf]
			// [c d]    [f]  -> [ce df]
			double[] eventsColumn = new double[totalStatesNumber];

			for(int m=0;m<totalStatesNumber;m++){
				double g =1.;

				if(false) {
					Gaussian gauss= new Gaussian(gaussianMeans[eventsId[j]][indexOfCphForState[m]], 
							Math.sqrt(gaussianVariances[eventsId[j]][indexOfCphForState[m]]));
					g = gauss.value(intensities[j]);
				}


				double val=M.get(indexOfCphForState[m], eventsId[j]);
				val= val*g;
				//					System.out.println(eventsId[j-1]+" s " +m+" "+val);



				eventsColumn[m]=val;
			}




			for(int m=0;m<totalStatesNumber;m++){
				for(int n=0; n<totalStatesNumber;n++) {
					newColumn.set(m, n, newColumn.get(m,n)* eventsColumn[n]);
				}
			}


			//			counter=0.;
			//			for(int k=0; k<statesNumber;k++){
			//				counter= counter+newColumn.get(k, 0);
			//			}
			//
			//			if(!(counter>0.)){
			//				System.out.println("Backward Probability sums after M to 0");
			//
			//			}




			newColumn = newColumn.mult(backwardArray[traceIndex].cols(j+1,j+2));


			Double counter=0.;
			for(int k=0; k<totalStatesNumber;k++){
				counter= counter+newColumn.get(k, 0);
			}
			//			newColumn = newColumn.divide(counter);

			if(!(counter>0.)){
				System.out.println("Backward Probability sums after end to 0");

			}


			backwardArray[traceIndex].insertIntoThis(0,j, newColumn);
		}


	}

	private SimpleMatrix  getSubV(int i, int l, int trace){

		SimpleMatrix PP = powerP0(l);

		//ricorda ad indice 0 sta fuori
		return forwardArray[trace].rows(i,i+1).mult(PP);
	}




	private SimpleMatrix  getSubW(int event, int exponent, int trace, int eventId){

		SimpleMatrix P1ModifiedByM = P1espanso.copy();


		// [a b] [e]  -> [ae bf]
		// [c d] [f]  -> [ce df]
		double[] eventsColumn = new double[totalStatesNumber];

		//SimpleMatrix eventsColumn=M.cols(eventId,eventId+1);

		for(int m=0;m<totalStatesNumber;m++){
			double val=M.get(indexOfCphForState[m], eventId);


			eventsColumn[m]=val;

		}

		for(int m=0;m<totalStatesNumber;m++){
			for(int n=0; n<totalStatesNumber;n++) {
				P1ModifiedByM.set(m, n, P1ModifiedByM.get(m,n)* eventsColumn[n]);
			}
		}

		SimpleMatrix PP = powerP0(exponent);
		return PP.mult(P1ModifiedByM).mult(backwardArray[trace].cols(event+1,event+2));
	}


	private SimpleMatrix getV(int i, int k, int trace){
		SimpleMatrix Vcon = new SimpleMatrix(totalStatesNumber,k); 
		for( int j=0; j<k; j++){
			Vcon.insertIntoThis(0,j, getSubV(i, j, trace).transpose());
		}
		return Vcon;
	}

	private SimpleMatrix getW(int i, int k, int trace, int eventId){
		SimpleMatrix Wcon = new SimpleMatrix(k,totalStatesNumber); 
		for( int j=0; j<k; j++){
			Wcon.insertIntoThis(j, 0, getSubW(i, k-1-j, trace, eventId).transpose());
		}
		return Wcon;
	}


	/**Attenzione restituisce la sommatoria delle prob di avere k movimenti sulla uniformized discreta
	 * MA moltiplicate per Po^(k-1) poich� � sempre seguito da P1!!!
	 * 
	 * */
	public SimpleMatrix unifP0loweredExponent(double time) {
		SimpleMatrix Unif = new SimpleMatrix(totalStatesNumber,totalStatesNumber);


		double at = uniformization*time;
		FoxGlynn fg= generateFoxGlynnReduced(at, UNIF_ERR);
		int leftB = fg.leftPoint();
		int rightB= fg.rightPoint();

		//		System.out.println(leftB);
		//		System.out.println(rightB);



		for(int k=leftB; k<rightB+1;k++) {
			double tmp = fg.poissonProb(k);

			if(k>0) {
				Unif = Unif.plus(powerP0(k-1).scale(tmp));	
			}
		}

		return Unif;
	}


	public SimpleMatrix unifP0normalExponent(double time) {
		SimpleMatrix Unif = new SimpleMatrix(totalStatesNumber,totalStatesNumber);

		//		System.out.println(uniformization);

		double at = uniformization*time;
		FoxGlynn fg= generateFoxGlynnReduced(at, UNIF_ERR);
		int leftB = fg.leftPoint();
		int rightB= fg.rightPoint();

		//		System.out.println(leftB);
		//		System.out.println(rightB);



		for(int k=leftB; k<rightB+1;k++) {
			double tmp = fg.poissonProb(k);

			Unif = Unif.plus(powerP0(k).scale(tmp));	

		}

		return Unif;
	}



	public SimpleMatrix powerP0(final int p) throws NonSquareMatrixException,
	NotPositiveException {
		if (p < 0) {
			throw new NotPositiveException(p);
		}

		if(powersP0.length>p && powersP0[p]!=null) {
			return powersP0[p];
		}

		if (p == 0) {
			SimpleMatrix s =new SimpleMatrix(P0.numRows(),P0.numCols());
			for(int i=0; i< P0.numRows();i++) {
				s.set(i, i, 1.00);
			}
			return s;
		}

		if (p == 1) {
			return P0.copy();
		}

		final int power = p - 1;

		/*
		 * Only log_2(p) operations is used by doing as follows:
		 * 5^214 = 5^128 * 5^64 * 5^16 * 5^4 * 5^2
		 *
		 * In general, the same approach is used for A^p.
		 */

		final char[] binaryRepresentation = Integer.toBinaryString(power)
				.toCharArray();
		final ArrayList<Integer> nonZeroPositions = new ArrayList<>();

		for (int i = 0; i < binaryRepresentation.length; ++i) {
			if (binaryRepresentation[i] == '1') {
				final int pos = binaryRepresentation.length - i - 1;
				nonZeroPositions.add(pos);
			}
		}

		ArrayList<SimpleMatrix> results = new ArrayList<>(
				binaryRepresentation.length);

		results.add(0, P0.copy());

		for (int i = 1; i < binaryRepresentation.length; ++i) {
			final SimpleMatrix s = results.get(i - 1);
			final SimpleMatrix r = s.mult(s);
			results.add(i, r);
		}

		SimpleMatrix result = P0.copy();

		for (Integer i : nonZeroPositions) {
			result = result.mult(results.get(i));
		}

		if(powersP0.length>p)
			powersP0[p]=result;

		return result;
	}

	public void powerP0array(SimpleMatrix[]arr) throws NonSquareMatrixException,
	NotPositiveException {

		final int p = arr.length-1;

		if (p < 0 ) {
			throw new NotPositiveException(p);
		}




		ArrayList<SimpleMatrix> results = new ArrayList<>(
				Integer.toBinaryString(p)
				.toCharArray().length);


		results.add(0, P0.copy());

		for (int i = 1; i < Integer.toBinaryString(p)
				.toCharArray().length; ++i) {
			final SimpleMatrix s = results.get(i - 1);
			final SimpleMatrix r = s.mult(s);
			results.add(i, r);
		}



		for( int k =0; k< p; k++) {

			if (k == 0) {
				SimpleMatrix s =new SimpleMatrix(P0.numRows(),P0.numCols());
				for(int i=0; i< P0.numRows();i++) {
					s.set(i, i, 1.00);
				}
				arr[k] = s;
			}else if (k == 1) {
				arr[k]= P0.copy();
			} else {

				final int power = k - 1;


				final char[] binaryRepresentation = Integer.toBinaryString(power)
						.toCharArray();

				/*
				 * Only log_2(p) operations is used by doing as follows:
				 * 5^214 = 5^128 * 5^64 * 5^16 * 5^4 * 5^2
				 *
				 * In general, the same approach is used for A^p.
				 */


				final ArrayList<Integer> nonZeroPositions = new ArrayList<>();

				for (int i = 0; i < binaryRepresentation.length; ++i) {
					if (binaryRepresentation[i] == '1') {
						final int pos = binaryRepresentation.length - i - 1;
						nonZeroPositions.add(pos);
					}
				}



				SimpleMatrix result = P0.copy();

				for (Integer i : nonZeroPositions) {
					result = result.mult(results.get(i));
				}

				arr[k]=result;
			}
		}
		return ;
	}


	public double getLogFitness() {
		return logfitness;
	}


	public int getEventTypes() {
		return eventTypes;
	}


	public LinkedList<String> getListEvents() {
		return listEvents;
	}


	public double[] getInitialProbabilities() {
		return initialProbabilities;
	}




	public double[][] getGaussianMeans() {
		return gaussianMeans;
	}




	public double[][] getGaussianVariances() {
		return gaussianVariances;
	}




	public double[][] getInitialProbCPH() {
		return initialProbCPH;
	}




	public int getTotalStatesNumber() {
		return totalStatesNumber;
	}




	public int getStatesPerCphNumber() {
		return statesPerCphNumber;
	}




	public int[] getIndexOfCphForState() {
		return indexOfCphForState;
	}




	public int getMacrostatesNumber() {
		return macrostatesNumber;
	}


	public List<LinkedList<InputTriple>> getDataset() {
		return dataset;
	}


	public boolean isCPH() {
		return statesPerCphNumber>1;
	}


	public SimpleMatrix getM() {
		return M;
	}


	public SimpleMatrix getInitialM() {
		return initialM;
	}


	public SimpleMatrix getInitialP0() {
		return initialP0;
	}


	public SimpleMatrix getInitialP1() {
		return initialP1macro;
	}


	public SimpleMatrix getD0() {
		return D0;
	}


	public SimpleMatrix getD1() {
		return D1macro;
	}

	public SimpleMatrix getP0() {
		return P0;
	}




	public SimpleMatrix getP1() {
		return P1macro;
	}

	public SimpleMatrix getP1espanso() {

		SimpleMatrix P1esp = new SimpleMatrix(totalStatesNumber,totalStatesNumber);
		for(int i=0;i<totalStatesNumber;i++) {
			double value= P1macro.get(indexOfCphForState[i], indexOfCphForState[i]);

			P1esp.set(i, i, value);

		}

		return P1esp;
	}


	public void print(String p) {

		File file = new File(p);
		try (PrintWriter writer = new PrintWriter(file)) {

			writer.println("counter= "+countRuns);
			writer.write("endR= ");
			writer.write("unif= ");
			writer.println(uniformization);

			writer.println();
			writer.printf("%5.2e", logfitness);
			writer.println();


			for(int i=0;i<listEvents.size();i++) {
				writer.write(listEvents.get(i)+",");
			}
			writer.println();
			writer.println();
			writer.write("M");
			writer.println();
			for(int i=0;i<M.numRows();i++) {
				for(int j=0;j<M.numCols();j++) {
					writer.printf("%5.2e", M.get(i, j));
					writer.print(",");
				}
				writer.println();
			}
			writer.println();
			writer.write("P0");
			writer.println();
			for(int i=0;i<P0.numRows();i++) {
				for(int j=0;j<P0.numCols();j++) {
					writer.printf("%5.2e", P0.get(i, j));
					writer.print(",");
				}
				writer.println();
			}
			writer.println();
			writer.write("P1");
			writer.println();
			for(int i=0;i<P1macro.numRows();i++) {
				for(int j=0;j<P1macro.numCols();j++) {
					writer.printf("%5.2e", P1macro.get(i, j));
					writer.print(",");
				}
				writer.println();
			}
			writer.println();
			writer.write("D0");
			writer.println();
			for(int i=0;i<D0.numRows();i++) {
				for(int j=0;j<D0.numCols();j++) {
					writer.printf("%5.2e", D0.get(i, j));
					writer.print(",");
				}
				writer.println();
			}
			writer.println();
			writer.write("D1");
			writer.println();
			for(int i=0;i<D1macro.numRows();i++) {
				for(int j=0;j<D1macro.numCols();j++) {
					writer.printf("%5.2e", D1macro.get(i, j));
					writer.print(",");
				}
				writer.println();
			}


		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}



	}


	//ERRORE QUI VUOLE SOMMARE TRAMITE MOLTIPLICAZIONE CON MATRICE RIGA 1, NON FUNZIONA PER MACRO!! 
	@SuppressWarnings("unused")
	private void printDebug(double fit, String p) {
		SimpleMatrix D01 = this.D0.copy();
		SimpleMatrix D11 = this.D1macro.copy();


		SimpleMatrix diag =new SimpleMatrix(totalStatesNumber,totalStatesNumber);
		SimpleMatrix eT = new SimpleMatrix(totalStatesNumber,1);
		eT=eT.plus(1.);
		SimpleMatrix sumM = P0.mult(eT);
		sumM= sumM.plus(P1macro.mult(eT));
		for(int i =0; i<totalStatesNumber;i++) {
			diag.set(i, i, sumM.get(i, 0));
		}

		D01 = P0.minus(diag);
		D01= D01.scale(uniformization);

		D11 = P1macro.scale(uniformization); 

		File file = new File(p);
		try (PrintWriter writer = new PrintWriter(file)) {

			writer.println();
			writer.write("initialD0");
			writer.println();
			for(int i=0;i<initialP0.numRows();i++) {
				for(int j=0;j<initialP0.numCols();j++) {
					writer.printf("%5.2e", initialP0.get(i, j));
					writer.print(",");
				}
				writer.println();
			}
			writer.println();
			writer.write("initialD1");
			writer.println();
			for(int i=0;i<initialP1macro.numRows();i++) {
				for(int j=0;j<initialP1macro.numCols();j++) {
					writer.printf("%5.2e", initialP1macro.get(i, j));
					writer.print(",");
				}
				writer.println();
			}

			writer.println();
			writer.write("initialM");
			writer.println();
			for(int i=0;i<initialM.numRows();i++) {
				for(int j=0;j<initialM.numCols();j++) {
					writer.printf("%5.2e", initialM.get(i, j));
					writer.print(",");
				}
				writer.println();
			}
			writer.write("endR= ");
			writer.write("unif= ");
			writer.println(uniformization);
			writer.write("Events= ");
			for(int i=0;i<listEvents.size();i++) {
				writer.write(listEvents.get(i)+",");
			}
			writer.println();
			writer.printf("%5.4e", fit);
			writer.println();

			writer.println();
			writer.println();
			writer.write("M");
			writer.println();
			for(int i=0;i<M.numRows();i++) {
				for(int j=0;j<M.numCols();j++) {
					writer.printf("%5.2e", M.get(i, j));
					writer.print(",");
				}
				writer.println();
			}
			writer.println();
			writer.write("P0");
			writer.println();
			for(int i=0;i<P0.numRows();i++) {
				for(int j=0;j<P0.numCols();j++) {
					writer.printf("%5.2e", P0.get(i, j));
					writer.print(",");
				}
				writer.println();
			}
			writer.println();
			writer.write("P1");
			writer.println();
			for(int i=0;i<P1macro.numRows();i++) {
				for(int j=0;j<P1macro.numCols();j++) {
					writer.printf("%5.2e", P1macro.get(i, j));
					writer.print(",");
				}
				writer.println();
			}
			writer.println();
			writer.write("currentD0");
			writer.println();
			for(int i=0;i<D01.numRows();i++) {
				for(int j=0;j<D01.numCols();j++) {
					writer.printf("%5.2e", D01.get(i, j));
					writer.print(",");
				}
				writer.println();
			}
			writer.println();
			writer.write("currentD1");
			writer.println();
			for(int i=0;i<D11.numRows();i++) {
				for(int j=0;j<D11.numCols();j++) {
					writer.printf("%5.2e", D11.get(i, j));
					writer.print(",");
				}
				writer.println();
			}


		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}



	}


	public static FoxGlynn generateFoxGlynnReduced(double lambdatime, double error) {
		double err=error;
		FoxGlynn fg= FoxGlynn.computeReduced(lambdatime, err);


		while(fg.rightPoint()<3 && err>10E-10) {
			err= err/10;
			fg= FoxGlynn.computeReduced(lambdatime, err);
		}


		return fg;
	}



	public Double getUniformization() {
		return uniformization;
	}

	public double[] getProbOfBeingInState() {
		return probOfBeingInState;
	}


}
