package dataset;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;

import modeling.InputTriple;



public class DatasetLoader {


	static public LinkedList<LinkedList<InputTriple>> loadFromCSV(String filename) {
		LinkedList<LinkedList<InputTriple>> dataset = new LinkedList<>();

		String csvFile = filename;
		String line = "";

		try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {

			while ((line = br.readLine()) != null) {

				LinkedList<InputTriple> list = new LinkedList<>();
				// use comma as separator
				String[] pieces = line.split(",");

				for(int i=0; i<pieces.length;i=i+3) {
					//double time double intensity string type  
					InputTriple p = new InputTriple(Double.valueOf(pieces[i]),Double.valueOf(pieces[i+1]),pieces[i+2]);
					list.add(p);
				}
				dataset.add(list);

			}

		} catch (IOException e) {
			e.printStackTrace();
		}


		return dataset;
	}	

	
}
