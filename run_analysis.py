#!/usr/bin/env python3

import argparse
from collections import Counter
from os import name
from pathlib import Path
import json
import yaml
import csv
import re
import numpy as np
import itertools
import pandas as pd
import plotly.graph_objects as go
from loguru import logger

def grouper(iterable, n):
    """Collect data into fixed-length chunks or blocks"""
    args = [iter(iterable)] * n
    return zip(*args)

def compute_seqs_stats(seqs_file):
    event_times = {}
    with open(seqs_file) as seqs_stream:

        reader = csv.reader(seqs_stream)
        for record in reader:
            sequence = grouper(record, 3)

            for interval, _, e in sequence:
                if e == 'end':
                    continue

                event_times[e] = event_times.get(e, [])
                event_times[e].append(int(interval))
    
    
    return {
        'event_intervals': { e: Counter(event_times[e]) for e in event_times.keys() },
        'event_frequencies': { e: len(event_times[e]) for e in event_times.keys() },
    }


def compute_tsung_stats(tsung_log_path):
    with open(tsung_log_path) as f:
        tsung_log_data = json.load(f)
    
    tsung_run_stats = {}

    for sample in tsung_log_data['stats']:
        if not sample['samples']:
            logger.info(f"Missing sample @ {tsung_log_path}")
            continue

        for sample_data in sample['samples']:
            measure = sample_data['name']
            if measure not in {'load', 'cpu', 'freemem'}:
                continue

            if not sample_data.get('hostname', None) or sample_data['hostname'] != 'scenario-detection-wildfly-1':
                continue

            tsung_run_stats[f'{measure}_mean_data'] = tsung_run_stats.get(f'{measure}_mean_data', [])
            tsung_run_stats[f'{measure}_mean_data'].append(float(sample_data['mean']))

            logger.info(f"{measure} data: {len(tsung_run_stats[f'{measure}_mean_data'])}")
    
    return tsung_run_stats


def analyze_runs(reference, data):
    reference = Path(reference)
    data = Path(data)
    datasets = {}


    logger.info(f"Loading reference from {reference}")
    logger.info(f"Loading data from {data}")

    for reference_dataset in reference.iterdir():
        with open(reference_dataset / 'info.yml', 'r') as info_file:
            metadata = yaml.safe_load(info_file)
            logger.info(f"Adding reference dataset {metadata['dataset']}")
            datasets[metadata['dataset']] = metadata
            datasets[metadata['dataset']]['runs'] = []
            datasets[metadata['dataset']]['tsung_runs_path'] = reference_dataset



    for model_folder in data.iterdir():
        logger.info(f"Entering model folder {model_folder}")
        info_files = model_folder.glob("*_info.yml")
        for info_file_path in info_files:
            with open(info_file_path, 'r') as info_file:
                model_meta = yaml.safe_load(info_file)

                # info_file format: `cut_N_info.yml`
                # generated sequences: `sequences_cut_N.csv`
                # Tsung folder: `sequences_cut_N`
                tsung_folder_suffix = re.sub('\_info$', '', info_file_path.stem)
                tsung_run_path = model_folder / f"sequences_{tsung_folder_suffix}"
                model_meta['tsung_path'] = tsung_run_path
                model_meta['generated_sequences'] = model_folder / f"sequences_{tsung_folder_suffix}.csv"

                datasets[model_meta['dataset']]['runs'].append(model_meta)

    for dataset_path, dataset in datasets.items():
        cuts = [p for p in Path(dataset_path).iterdir() if p.stem.endswith('test')]
        logger.info(f"Cuts for {dataset_path}: {cuts}")

        dataset_stats = {}

        for cut_file in cuts:
            cut_number = int(re.search('(\d+)_.*', cut_file.stem).group(1))
            dataset_stats[cut_number] = compute_seqs_stats(cut_file)

            tsung_cut_runs = (p for p in (dataset['tsung_runs_path'] / cut_file.stem).iterdir() if p.is_dir())
            tsung_runs_stats = {}

            for tsung_run in tsung_cut_runs:
                logger.info(f"Opening {tsung_run} log")
                tsung_log_path = tsung_run / "tsung.log"

                tsung_runs_stats[tsung_run] = compute_tsung_stats(tsung_log_path)
            dataset_stats[cut_number]['tsung_stats'] = tsung_runs_stats
        
        dataset['reference_stats'] = dataset_stats
    
    for dataset in datasets.values():
        for run in dataset['runs']:
            run['stats'] = compute_seqs_stats(run['generated_sequences'])

            tsung_cut_runs = (p for p in run['tsung_path'].iterdir() if p.is_dir())
            tsung_runs_stats = {}

            for tsung_run in tsung_cut_runs:
                tsung_log_path = tsung_run / "tsung.log"
                logger.info(f"Opening {tsung_log_path}...")

                tsung_runs_stats[tsung_run] = compute_tsung_stats(tsung_log_path)
            run['tsung_stats'] = tsung_runs_stats

    # Tsung run stats:
    # d[DATASET_PATH]['runs'][RUN_NO]['tsung_stats'][TSUNG_RUN_PATH]
    # corresponding reference(s) stats:
    # d[DATASET_PATH]['reference_stats'][RUN_NO]['tsung_stats'][TSUNG_REF_RUN_PATH]

    return datasets

def compute_tsung_runs_means(measures, runs):
    runs_data = { m: { 'mean': [] } for m in measures }

    for run in runs:
        for tsung_run in run['tsung_stats'].values():
            for measure in measures:
                runs_data[measure]['mean'].append(tsung_run[f"{measure}_mean_data"])
    
    runs_data_full = {}
    for m in measures:
        mean = np.mean(runs_data[m]['mean'], axis=0)
        mean = mean[mean != 0]

        stdev = np.std(runs_data[m]['mean'], axis=0)

        runs_data_full[m] = {
            'mean': mean,
            'up': mean + stdev,
            'low': mean - stdev
        }

    return runs_data_full

def make_measure_plot(measure, data, title):
    x = list(range(len(data['mean'])))

    color_gen = lambda t: f"rgba(25, 118, 210, {t})"

    fig = go.Figure()
    fig.add_trace(go.Scatter(
        x=x + x[::-1],
        y=list(data['up']) + list(data['low'])[::-1],
        fill='toself',
        fillcolor=color_gen(0.2),
        line_color='rgba(0, 0, 0, 0)',
        showlegend=False,
        name=f'{measure}'
    ))

    fig.add_trace(go.Scatter(
        x=x,
        y=list(data['mean']),
        line_color=color_gen(1),
        name=f'{measure}'
    ))

    fig.update_layout(
        title=title,
        xaxis_title="samples",
        # yaxis_title="Y Axis Title",
    )

    return fig

def plot_stats(results, output_path):
    output_path = Path(output_path)
    output_path.mkdir(exist_ok=True)

    measures = {"cpu", "load", "freemem"}

    for dataset_path, dataset_data in results.items():
        macrostates_run = set(run['macrostates'] for run in dataset_data['runs'])

        grouping_key = lambda r: f"{r['macrostates']}_{r['cut']}"
        runs_by_macrostates = itertools.groupby(sorted(dataset_data['runs'], key=grouping_key), key=grouping_key)

        for run_descriptor, runs in runs_by_macrostates:
            run_macrostates, run_cut = run_descriptor.split('_')
            run_macrostates, run_cut = int(run_macrostates), int(run_cut)

            runs_data_full = compute_tsung_runs_means(measures, runs)

            with open(output_path / f"{Path(dataset_path).name}_cut{run_cut}_states{run_macrostates}.yml", 'w') as f:
                serializable_runs_data = { m: { k: runs_data_full[m][k].tolist() for k in runs_data_full[m] } for m in measures }
                yaml.dump(serializable_runs_data, f)

            for measure in measures:
                data = runs_data_full[measure]
                fig = make_measure_plot(measure,
                    data,
                    title=f"{measure} utilization plot for model from cut {run_cut} with {run_macrostates} states from dataset {Path(dataset_path).name}")

                fig.write_image(output_path / f"{Path(dataset_path).name}_cut{run_cut}_states{run_macrostates}_{measure}.pdf")
        
        # d[DATASET_PATH]['reference_stats'][RUN_NO]['tsung_stats'][TSUNG_REF_RUN_PATH]

        reference_tsung_means = compute_tsung_runs_means(measures, dataset_data['reference_stats'].values())
        for measure in measures:
            data = reference_tsung_means[measure]
            fig = make_measure_plot(measure,
                data,
                title=f"Reference {measure} utilization plot from dataset {Path(dataset_path).name}")

            fig.write_image(output_path / f"{Path(dataset_path).name}_reference_{measure}.pdf")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-r', '--reference', required=True, type=Path, metavar='FOLDER',
                        help='Folder with tsung runs from reference dataset (ie testing dataset for validation or full dataset for fitting).\n'
                        'Folder structure: FOLDER/DATASET/CUT_FOLDER/TSUNG_RUN/tsung.log. Tsung output must be in JSON.')
    parser.add_argument('-d', '--data', required=True, type=Path, metavar='DATA',
                        help='Folder containing tsung runs and model parameters.\n'
                        'Folder structure: DATA/INFERENCE_RUN/CUT_FOLDER/TSUNG_RUN/tsung.log. Tsung output must be in JSON.')
    parser.add_argument('-o', '--output', required=True, help='Analysis output path', type=Path)
    args = parser.parse_args()

    analysis_results = analyze_runs(args.reference, args.data)

    analysis_output_path = args.output
    analysis_output_path.mkdir(exist_ok=True)

    plot_stats(analysis_results, analysis_output_path)
