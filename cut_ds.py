import csv
from pathlib import Path
from urllib import parse
import argparse
from random import shuffle
from math import ceil, floor

parser = argparse.ArgumentParser()
parser.add_argument('-k', '--cuts', type=int, required=True)
parser.add_argument('-d', '--dataset', required=True, type=Path)
parser.add_argument('-o', '--output-path', required=True, type=Path)
parser.add_argument('-t', '--has-test', action=argparse.BooleanOptionalAction, default=True)

args = parser.parse_args()


DATASET_PATH = args.dataset
DATASET_OUTPUT_PATH = args.output_path

DATASET_OUTPUT_PATH.mkdir(exist_ok=True)
datasets = DATASET_PATH.glob("*.csv")

SPLIT_TEST = args.has_test
CUTS = args.cuts


def grouper(iterable, n):
    """Collect data into fixed-length chunks or blocks"""
    args = [iter(iterable)] * n
    return zip(*args)

print(f"{SPLIT_TEST=}")

for dataset in datasets:
    with open(dataset) as ds:
        records = sum(1 for _ in ds)

    shuffled_indices = list(range(records))
    shuffle(shuffled_indices)

    if CUTS > 1:
        cut_indices = [(int(k * floor(records / CUTS)), int((k + 1) * ceil(records / CUTS))) for k in range(CUTS)]
        cut_indices[-1] = (cut_indices[-1][0], records)
    else:
        cut_indices = [(0, 0) for _ in range(CUTS)]

    print(f"DEBUG: {dataset.name} {cut_indices=}")

    for cut in range(CUTS):
        if SPLIT_TEST:
            excluded_indices = set(shuffled_indices[cut_indices[cut][0]:cut_indices[cut][1]])
        else:
            excluded_indices = set(shuffled_indices)

        Path(DATASET_OUTPUT_PATH / dataset.stem).mkdir(exist_ok=True)

        output_path = DATASET_OUTPUT_PATH / dataset.stem / f"{cut}_data.csv"
        test_output_path = DATASET_OUTPUT_PATH / dataset.stem / f"{cut}_test.csv"

        with open(dataset) as ds, open(output_path, 'w') as output, open(test_output_path, 'w') as test_output:
            reader = csv.reader(ds)
            writer = csv.writer(output)
            test_writer = csv.writer(test_output)

            for num_record, record in enumerate(reader):
                if num_record not in excluded_indices:
                    writer.writerow(record)
                else:
                    test_writer.writerow(record)

                    if not SPLIT_TEST:
                        print(f"Warning: SPLIT_TEST == False but record {num_record} is being written!")
