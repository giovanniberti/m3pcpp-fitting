#!/usr/bin/env python3

import subprocess
import argparse
from pathlib import Path
import os
from datetime import datetime, timedelta
import sys
from tsung_gen import generate_tsung_conf
import dotenv
from pushover import Client

dotenv.load_dotenv()
pushover_client = Client(os.environ["PUSHOVER_USER_KEY"], api_token=os.environ["PUSHOVER_APP_KEY"])

def send_message(message, title):
	try:
		pushover_client.send_message(message=message, title=title)
	except:
		pass

def main():
	parser = argparse.ArgumentParser()
	parser.add_argument('-d', '--dataset', required=True, type=Path)
	parser.add_argument('-k', '--cuts', required=True, type=int)
	args = parser.parse_args()

	DATASET_PATH = args.dataset
	states = list(range(4, 9))

	datasets = list(DATASET_PATH.iterdir())

	subprocess.run(['mvn', 'clean', 'install'], check=True)
	parameter_output_path = Path("output")

	for dataset in datasets:
		print("processing", dataset)
		for state in states:
			try:
				send_message(f"Dataset: {dataset.name}, states: {state}", title="Starting training")

				run_folder_name = datetime.now().isoformat()
				run_folder = parameter_output_path / run_folder_name
				subprocess.run(['mvn',
					'exec:java',
					'-Dexec.mainClass=modeling.Main',
					f'-Dexec.args=-k {args.cuts} '
					f'-m {state} '
					'-f 0.04 '
					f'-d \'{dataset}\' '
					'-n 100 '
					f'-o {parameter_output_path} '
					f'-i {run_folder_name} '],
					env={
						**os.environ,
						'MAVEN_OPTS': '-ea'
					},
					check=True)
				generation_paths = generate_tsung_conf(dataset_path=run_folder, output_path=run_folder, run_time=timedelta(minutes=15))
				print(f"Generated tsung.xml in {generation_paths}")

				pushover_client.send_message(f"Dataset: {dataset.name}, states: {state}", title="Starting load test")

				for path in generation_paths:

					print(f"Starting Tsung load test ({path})...")
					sys.stdout.flush()

					tsung_start_time = datetime.utcnow()
					subprocess.run([
						'docker',
						'run',
						'-it',
						'--network=scenario-detection_default',
						'--mount',
						f'type=bind,source={path.resolve()},destination=/usr/local/tsung',
						'tsung',
						'-f',
						'/usr/local/tsung/tsung.xml',
						'start'
					],
					check=True)

					tsung_load_folder = path / f"{tsung_start_time:%Y%m%d-%H%M}"
					tsung_log = tsung_load_folder / "tsung.log"
			except Exception as e:
				send_message(f"Error on {dataset.name}: {e}", title="Error")
				print(e)
				continue

			send_message(f"Dataset: {dataset.name}, states: {state}", title="Ended load test")

	send_message(f"Finished training on {DATASET_PATH}", title="Finished training")

if __name__ == '__main__':
	try:
		main()
	except Exception as e:
		send_message("Error on training", title="Training error")
		raise e