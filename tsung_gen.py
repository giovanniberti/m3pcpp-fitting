import csv
from datetime import timedelta
from math import ceil
from pathlib import Path
from io import StringIO
import lxml.etree
import lxml.builder
import argparse

def grouper(iterable, n):
    """Collect data into fixed-length chunks or blocks"""
    args = [iter(iterable)] * n
    return zip(*args)


def generate_tsung_conf(dataset_path, output_path=Path("tsung_data"), run_time=timedelta(minutes=2), dataset_filter=lambda x: True):
    E = lxml.builder.ElementMaker()

    output_path.mkdir(exist_ok=True)
    datasets = filter(dataset_filter, dataset_path.glob("*.csv"))

    # TODO: Check URL parameters
    URL_EVENT_MAPPINGS = {
        0: "/services/channel/deletevideo?videoUUID=00000000-0000-0000-0000-000000000000", # "DELETE_VIDEO",
        1: "/search", # "SEARCH",
        2: "/services/videos/watch?videoUUID=00000000-0000-0000-0000-000000000000", # "VIEW_VIDEO",
        3: "/services/channel/viewchannel?chUUID=00000000-0000-0000-0000-000000000000", # "VIEW_CHANNEL",
        4: "/services/users/login", # "LOGIN",
        5: "/services/videos/like?videoUUID=70e175cf-32d3-4b52-b208-9059bc165e5c", # "LIKE",
        6: "/services/videos/comment?videoUUID=70e175cf-32d3-4b52-b208-9059bc165e5c&comment=", # "COMMENT",
        7: "/services/channel/subscribe?usrUUID=00000000-0000-0000-0000-000000000000&chUUID=00000000-0000-0000-0000-000000000000", # "SUBSCRIBE",
        8: "/services/channel/uploadvideo?videoname=&descriptor=&channelUUID=00000000-0000-0000-0000-000000000000", # "UPLOAD_VIDEO",
        9: "/services/users/logout", # "LOGOUT",
        10: "/services/videos/like?videoUUID=70e175cf-32d3-4b52-b208-9059bc165e5c", # "DISLIKE",
        11: "/services/channel/subscribe?chUUID=00000000-0000-0000-0000-000000000000", # "UNSUBSCRIBE",
        12: "/services/videos/rate?videoUUID=70e175cf-32d3-4b52-b208-9059bc165e5c&rating=5", # "RATING",
        13: "/services/videos/stats?videoUUID=70e175cf-32d3-4b52-b208-9059bc165e5c", # "STATS"
    }


    Tsung = E.tsung
    Options = E.options
    Option = E.option
    Sessions = E.sessions
    Session = E.session
    Clients = E.clients
    Client = E.client
    Servers = E.servers
    Server = E.server
    Load = E.load
    ArrivalPhase = E.arrivalphase
    Users = E.users
    Request = E.request
    Http = E.http
    ThinkTime = E.thinktime
    Monitoring = E.monitoring
    Monitor = E.monitor
    Snmp = E.snmp

    # TODO: Add setup phase with login?

    s = """<?xml version="1.0"?>
    <!DOCTYPE tsung SYSTEM "/usr/share/tsung/tsung-1.0.dtd">
    <tsung></tsung>
    """

    output_sessions = Sessions()
    output_conf = lxml.etree.parse(StringIO(s))
    root = output_conf.getroot()
    root[:] = Tsung(
        Clients(
            Client(host="localhost", use_controller_vm="true", maxusers="5000"),
        ),
        Servers(
            Server(host="scenario-detection-wildfly-1", port="8080", type="tcp")
        ),
        Monitoring(
            Monitor(host="scenario-detection-wildfly-1", type="erlang")
        ),
        Load(
            ArrivalPhase(Users(arrivalrate="4", unit="second"), phase="1", duration=str(run_time.seconds), unit="second"),
            duration=str(run_time.seconds),
            unit="second"
        ),
        output_sessions)
    root.attrib["loglevel"] = "debug"
    root.attrib["backend"] = "json"

    generation_paths = []

    for dataset in datasets:
        generation_base_path = output_path / dataset.stem
        generation_base_path.mkdir(exist_ok=True)

        generation_paths.append(generation_base_path)

        conf_output_path = generation_base_path / "tsung.xml"
        output_sessions.clear()

        print("Writing to", conf_output_path)
        print("Opening", dataset)

        with open(conf_output_path, 'w') as output:
            with open(dataset) as ds:
                num_records = sum(1 for _ in iter(ds))

            with open(dataset) as ds:
                reader = csv.reader(ds)
                for i, record in enumerate(reader):
                    # record = record[3:]
                    sequence = grouper(record, 3)
                    output_sequence = []
                    requests = []

                    for ts, _, e in sequence:
                        if e == 'end':
                            continue

                        ts = int(round(float(ts))) / 1000
                        interevent_time = ts
                        output_sequence.append(str(interevent_time))
                        output_sequence.append(e)
                        requests.append(Request(Http(url=f"/scenedet{URL_EVENT_MAPPINGS[int(e)]}", method='GET', version='1.1')))
                        requests.append(ThinkTime(value=str(interevent_time), random='false'))

                    output_sessions.append(Session(
                        name=f"{str(dataset.stem).replace(' ', '_').replace('.', '_')}_{i}",
                        weight=str(num_records),
                        type="ts_http",
                        *requests
                    ))
                
            output.write(str(lxml.etree.tostring(output_conf, pretty_print=True, xml_declaration=True, encoding='utf-8'), 'ascii'))

        print("Written Tsung configuration to", conf_output_path)
    
    return generation_paths


if __name__ == "main":
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--dataset', required=True, type=Path)

    args = parser.parse_args()
    generate_tsung_conf(dataset_path=args.dataset)
