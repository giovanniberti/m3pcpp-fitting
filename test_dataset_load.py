#!/usr/bin/env python3

import subprocess
import argparse
from pathlib import Path
import os
from datetime import datetime, timedelta
import sys
from tsung_gen import generate_tsung_conf
import dotenv
from pushover import Client

dotenv.load_dotenv()
pushover_client = Client(os.environ["PUSHOVER_USER_KEY"], api_token=os.environ["PUSHOVER_APP_KEY"])

def send_message(message, title):
	try:
		pushover_client.send_message(message=message, title=title)
	except:
		pass

"""
Script purpose: run reference load test from folder containing datasets
"""

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--dataset', required=True, type=Path)
    parser.add_argument('-o', '--output', required=True, type=Path)
    args = parser.parse_args()

    DATASET_PATH = args.dataset

    datasets = DATASET_PATH.iterdir()

    for dataset in datasets:
        print("processing", dataset)

        conf_output_path = args.output / dataset.name
        conf_output_path.mkdir(parents=True, exist_ok=True)

        total_csvs = 2
        with open(conf_output_path / "info.yml", 'w') as info_file:
            total_csvs = len(list(dataset.glob('*.csv')))

            info_file.write(f"dataset: {dataset.resolve()}\n")
            info_file.write(f"total_cuts: {total_csvs / 2}\n")
        
        dataset_filter = lambda p: True
        if total_csvs > 2:
            # More than one cut => generate load only on test data
            dataset_filter = lambda p: "test" in p.name

        print("Generating tsung.xml from", dataset)
        generation_paths = generate_tsung_conf(dataset_path=dataset,
            output_path=conf_output_path,
            run_time=timedelta(minutes=15),
            dataset_filter=dataset_filter)
        print(f"Generated tsung.xml in {generation_paths}")

        send_message(f"Dataset: {dataset.name}", title="Starting load test")

        for path in generation_paths:

            print(f"Starting Tsung load test ({path})...")
            sys.stdout.flush()

            tsung_start_time = datetime.utcnow()
            subprocess.run([
                'docker',
                'run',
                '-it',
                '--network=scenario-detection_default',
                '--mount',
                f'type=bind,source={path.resolve()},destination=/usr/local/tsung',
                'tsung',
                '-f',
                '/usr/local/tsung/tsung.xml',
                'start'
            ],
            check=True)

        send_message(f"Dataset: {dataset.name}", title="Ended load test")

    send_message(f"Finished training on {DATASET_PATH}", title="Finished training")

if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        send_message(f"Error on training:\n{e}", title="Training error")
        raise e
